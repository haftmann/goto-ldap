
'''
    Explicit LDAP configuration handling, without relying on the
    imponderabilities of config file lookup order etc.
'''

# Copyright 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Config', 'parse_raw', 'safe_ldap_option_setting_using', 'check_ldap_options']


from contextlib import contextmanager
import os

import ldap

from mopynaco import text
from mopynaco.Record import Record
from mopynaco import pathutil


default_timeout = 30


# dynamic lookup of default configuration on import

options_fixed = { # these options are fixed regardless of their runtime default
  ldap.OPT_HOST_NAME: ''
}

options_read_only = [ # these options cannot by set and are thus skipped
  ldap.OPT_API_INFO,
  ldap.OPT_X_TLS_PACKAGE
]

@contextmanager
def bare_ldap_defaults():
    previous = os.environ.get('LDAPNOINIT')
    os.environ['LDAPNOINIT'] = 'true'
    try:
        yield
    finally:
        del os.environ['LDAPNOINIT']
        if previous is not None:
            os.environ['LDAPNOINIT'] = previous

option_defaults = {}
option_names = {}
with bare_ldap_defaults():
    for name in dir(ldap):
        if name.startswith('OPT_') and name != 'OPT_NAMES_DICT':
            opt = getattr(ldap, name)
            option_names.setdefault(opt, []).append(name)
            if opt in options_read_only:
                continue
            if opt in options_fixed:
                value = options_fixed[opt]
            else:
                try:
                    value = ldap.get_option(opt)
                except ValueError:
                    continue
                if value is None:
                    continue
            option_defaults[opt] = value

def safe_ldap_option_setting_using(f, opt, value):
    try:
        names = option_names.get(opt, [])
        f(opt, value)
        _ = str(opt)
          ## this is required to really propagate an exception occuring during f(…, …)
          ## (bad implementation in python-ldap?)
    except ValueError:
        msg = ('Unable to set (one of) LDAP option(s) {} -- consider adding them '
          'to list of read-only options in module {}').format(', '.join(names), __name__)
        raise Exception(msg)

def check_ldap_options():

    for opt in option_defaults:
        safe_ldap_option_setting_using(ldap.set_option, opt, option_defaults[opt])


# raw parsing of config files

loc_system_config = '/etc/ldap/ldap.conf'

def parse_raw(loc):

    raw = dict()
    if pathutil.is_readable_file(loc):
        with text.reading_utf8(loc) as reader:
            content = reader.read()
        for line in content.split('\n'):
            line = line.strip()
            if not line or line.startswith('#'):
                continue
            fields = line.split(' ', 1)
            key, value = fields if len(fields) == 2 else (fields[0], '')
            key = key.upper()
            value = value.strip()
            raw[key] = value

    return raw


# mapping of custom options

class Option_Spec(Record('symbolic', 'setting', 'parse', 'unparse', 'check', 'uncheck', 'default')):
    pass

def tap(f):
    def _tap(x):
        f(x)
        return x
    return _tap

options_custom = dict(
  uris = Option_Spec(
    symbolic = ldap.OPT_URI,
    setting = 'URI',
    parse = lambda txt: txt,
    unparse = lambda txt: txt,
    check = lambda txt: txt.split(),
    uncheck = lambda uris: ' '.join(uris),
    default = []
  ),
  timeout = Option_Spec(
    symbolic = ldap.OPT_TIMEOUT,
    setting = 'TIMEOUT',
    parse = int,
    unparse = str,
    check = tap(lambda timeout: isinstance(timeout, int)),
    uncheck = tap(lambda timeout: isinstance(timeout, int)),
    default = default_timeout
  ),
  referrals = Option_Spec(
    symbolic = ldap.OPT_REFERRALS,
    setting = 'REFERRALS',
    parse = lambda txt: txt in ('true', 'yes', 'on'),
    unparse = lambda b: 'yes' if b else 'no',
    check = tap(lambda referrals: isinstance(referrals, bool)),
    uncheck = tap(lambda referrals: isinstance(referrals, bool)),
    default = False
  ),
  protocol = Option_Spec(
    symbolic = ldap.OPT_PROTOCOL_VERSION,
    setting = 'VERSION',
    parse = int,
    unparse = str,
    check = tap(lambda v: v in (2, 3)),
    uncheck = tap(lambda v: v in (2, 3)),
    default = 3
  )
)


class Config:

    def __init__(self, **kwargs):

        assert all(name in options_custom for name in kwargs)

        for name in options_custom:
            spec = options_custom[name]
            value = kwargs[name] if name in kwargs else spec.default
            setattr(self, name, value)

    def update(self, **kwargs):

        for name in options_custom:
            if name not in kwargs:
                kwargs[name] = getattr(self, name)
        return self.__class__(**kwargs)

    @classmethod
    def from_file(Class, loc, **kwargs):

        raw = parse_raw(loc)
        for name in options_custom:
            spec = options_custom[name]
            if spec.setting in raw:
                kwargs[name] = spec.check(spec.parse(raw[spec.setting]))
        return Class(**kwargs)

    @classmethod
    def from_system(Class, **kwargs):

        return Class.from_file(loc_system_config, **kwargs)

    def raw_options(self):

        options = dict((opt, option_defaults[opt]) for opt in option_defaults)
        for name in options_custom:
            spec = options_custom[name]
            options[spec.symbolic] = spec.uncheck(getattr(self, name))
        return options

    def __repr__(self):

        return self.__class__.__name__ + '(' \
          + ', '.join('{0} = {1}'.format(name, repr(getattr(self, name))) for name in sorted(options_custom)) \
          + ')'
