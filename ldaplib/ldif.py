
'''
    Pythonic LDIF parsing and printing.
'''

# Copyright 2014 - 2017 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['internalize', 'convert_binary']


from ldif import LDIFParser, LDIFWriter
from io import StringIO

from mopynaco import text


def convert_binary(some_dn, attr_name, s):

    try:
        return text.of_utf8(s)
    except UnicodeDecodeError as e:
        pretty_dn = 'DN "{0}"'.format(some_dn) if some_dn is not None else 'unknown DN'
        raise UnicodeDecodeError(e.encoding, e.object, e.start, e.end,
          'Binary data in LDAP attribute "{0}" for {1}'.format(attr_name,
            pretty_dn))


def internalize(txt, is_binary_attribute = lambda _: False):

    assert isinstance(txt, str)

    reader = StringIO(txt)

    class Internalizer(LDIFParser):
        def __init__(self):
            LDIFParser.__init__(self, reader)
            self.some_dn = None
            self.result = dict()
        def handle(self, dn, entry):
            if dn is not None:
                self.some_dn = dn
            self.result = entry

    internalizer = Internalizer()
    internalizer.parse()

    some_dn = internalizer.some_dn
    result_text = {'dn': [some_dn] if some_dn is not None else []}
    result_binary = {}
    for key, raw_values in internalizer.result.items():
        if not is_binary_attribute(key):
            result_text[key] = [convert_binary(some_dn, key, raw_value) for raw_value in raw_values]
        else:
            result_binary[key] = raw_values
    return result_text, result_binary


def externalize(writer, dn, attrs, blobs):

    externalizer = LDIFWriter(writer, base64_attrs = blobs.keys())
    entry = {key: [text.utf8_of(value) for value in attrs[key]] for key in attrs}
    entry.update({key: blobs[key] for key in blobs})
    externalizer.unparse(dn, entry)
