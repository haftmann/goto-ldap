
'''
    Specification for segments of an LDAP tree.
'''

# Copyright 2016 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Struct']


from .Dn import Dn


class Struct:

    def __init__(self, object_class = None, attrs = [], blobs = [], children = {}):

        assert isinstance(object_class, (type(None), str))
        assert all(isinstance(name, str) for name in attrs)
        assert all(isinstance(name, str) for name in blobs)
        assert all(isinstance(name, str) for name in children)
        assert all(isinstance(struct, Struct) for struct in list(children.values()))

        self.object_class = object_class
        self.attrs = attrs
        self.blobs = blobs
        self.children = {str(name): children[name] for name in children}

    def structurize(self, base, raw_data):

        assert isinstance(base, Dn)

        result = {}

        for text_dn, (attrs, blobs) in raw_data:
            _, _, rev_rdns = base.common_prefix_suffixes(Dn.from_text(text_dn))
            rdns = list(reversed(rev_rdns))

            leaf_struct = self
            for key, value in rdns:
                if key not in leaf_struct.children:
                    leaf_struct = None
                    break
                leaf_struct = leaf_struct.children[key]

            if leaf_struct is None:
                continue

            if leaf_struct.object_class is not None \
              and leaf_struct.object_class not in attrs['objectClass']:
                continue

            if len(rdns) > 0:
                leaf_result = {}
                sub_result = result
                for key, value in rdns[:-1]:
                    sub_result = sub_result[key][value]
                key, value = rdns[-1]
                sub_result.setdefault(key, {})[value] = leaf_result
            else:
                leaf_result = result

            for name in leaf_struct.attrs:
                leaf_result[name] = attrs[name]
            for name in leaf_struct.blobs:
                leaf_result[name] = blobs[name]
            for name in leaf_struct.children:
                leaf_result[name] = dict()

        return result

    def all_attrs(self):

        for name in self.attrs:
            yield name

        for struct in list(self.children.values()):
            for name in struct.all_attrs():
                yield name

    def all_blobs(self):

        for name in self.blobs:
            yield name

        for struct in list(self.children.values()):
            for name in struct.all_blobs():
                yield name

    def __str__(self):

        return 'Struct({}attrs = {}, blobs = {}, children = dict({}))' \
          .format('' if self.object_class is None else '<{}> '.format(self.object_class),
            str(self.attrs), str(self.blobs),
            ', '.join('{} = {}'.format(name, str(self.children[name])) for name in self.children))
