
'''
    Auxiliary for (LDAP) data processing.
'''

# Copyright 2017 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['some_elem', 'the_value', 'boolify', 'partial_dict', 'diff_records']


from mopynaco import seq


def some_elem(xs, default = None):

    return default if seq.null(xs) else xs[0]


def the_value(d, key, default = None):

    values = d[key]
    if len(values) == 1:
        return values[0]
    elif len(values) == 0:
        if default is not None:
            return default
        else:
            raise ValueError('No value for attribute »{}«'.format(key))
    else:
        raise ValueError('Multiple values for attribute »{}«'.format(key))


def boolify(b):

    return 'true' if b else ''


def partial_dict(**kwargs):

    return {key: value for key, value in kwargs.items() if value is not None}


def diff(old, new):

    return {key: new[key] for key in new if old.get(key, []) != new[key]}


def diff_records(existing, wanted):

    attrs_blobs, diff_attrs_blobs = {}, {}
    for record in wanted:
        dn, (attrs, blobs) = record
        if dn not in existing:
            attrs_blobs[dn] = (attrs, blobs)
        else:
            old_attrs, old_blobs = existing.pop(dn)
            diff_attrs = diff(old_attrs, attrs)
            diff_blobs = diff(old_blobs, blobs)
            if diff_attrs or diff_blobs:
                diff_attrs_blobs[dn] = (diff_attrs, diff_blobs)
    create = [(dn, attrs_blobs[dn])
      for dn in sorted(attrs_blobs, key = lambda dn: dn.order_key())]
    change = [(dn, diff_attrs_blobs[dn])
      for dn in sorted(diff_attrs_blobs, key = lambda dn: dn.order_key())]
    remove = sorted(set(existing),
      key = lambda dn: dn.order_key(), reverse = True)

    return create, change, remove
