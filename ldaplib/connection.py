
'''
    Really pythonic interfaces for python-pyldap.
'''

# Copyright 2014 - 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['connection']


from contextlib import contextmanager

import ldap
import ldap.ldapobject

from mopynaco import seq
from mopynaco import text

from .Config import safe_ldap_option_setting_using
from .Config import Config
from .Dn import Dn
from . import filter as filter_expr
from . import ldif
from .aux import diff_records
from .Struct import Struct
from . import exceptions


class Ldap_Connection:

    def __init__(self, uris, internal_connection):

        self.uris = uris
        self.internal_connection = internal_connection

    def do_query(self, base, scope, filter, attrs, blobs, timeout):

        assert isinstance(base, Dn)
        assert isinstance(filter, filter_expr.Abstract)
        assert attrs is Ellipsis or all(isinstance(attr, str) for attr in attrs)
        assert blobs is Ellipsis or all(isinstance(attr, str) for attr in blobs)
        assert blobs is not Ellipsis or attrs is Ellipsis

        method = self.internal_connection.search_s if timeout is None \
          else self.internal_connection.search_st
        kw_timeout = dict() if timeout is None else dict(timeout = timeout)
        raw_attrs = (attrs + (blobs if blobs is not Ellipsis else [])) \
          if attrs is not Ellipsis else None
        dn_only = attrs is not Ellipsis and seq.null(raw_attrs)

        try:
            result = method(str(base), scope, str(filter), raw_attrs,
              dn_only, **kw_timeout)
        except ldap.NO_SUCH_OBJECT:
            return
        except ldap.SERVER_DOWN:
            raise exceptions.No_Connection(self.uris, base)

        for dn, raw_entry in result:
            if dn is None:
                continue # skip referrals -- not caught by python-ldap
            if dn_only:
                yield dn, (dict(), dict())
            else:
                if raw_attrs is None:
                    actual_raw_attrs = list(raw_entry)
                else:
                    raw_attr_mapping = {key.lower(): key for key in raw_entry}
                    actual_raw_attrs = [raw_attr_mapping.get(key.lower(), key) for key in raw_attrs]

                result = dict()
                result_blob = dict()
                for attr in actual_raw_attrs:
                    raw_values = list(raw_entry.get(attr, []))
                    if blobs is Ellipsis or attr in blobs:
                        result_blob[attr] = raw_values
                    else:
                        result[attr] = [ldif.convert_binary(dn, attr, raw_value) for raw_value in raw_values]
                yield dn, (result, result_blob)

    def query(self, base, filter = filter_expr.anything, attrs = Ellipsis, blobs = [], timeout = None):

        return self.do_query(base, ldap.SCOPE_BASE, filter, attrs, blobs, timeout)

    def query_children(self, base, filter = filter_expr.anything, attrs = Ellipsis, blobs = [], timeout = None):

        return self.do_query(base, ldap.SCOPE_ONELEVEL, filter, attrs, blobs, timeout)

    def query_tree(self, base, filter = filter_expr.anything, attrs = Ellipsis, blobs = [], timeout = None):

        return self.do_query(base, ldap.SCOPE_SUBTREE, filter, attrs, blobs, timeout)

    def query_structure(self, base, struct, timeout = None):

        assert isinstance(struct, Struct)

        raw_data = list(self.query_tree(base,
          attrs = list(struct.all_attrs()) + ['objectClass'],
          blobs = list(struct.all_blobs())))

        return struct.structurize(base, raw_data)

    def create(self, base, attrs = {}, blobs = {}):

        assert isinstance(base, Dn)
        assert isinstance(attrs, dict) and all(isinstance(key, str) for key in attrs) \
          and all(isinstance(values, list) and all(isinstance(value, str) for value in values) \
            for values in attrs.values())
        assert isinstance(blobs, dict) and all(isinstance(key, str) for key in blobs) \
          and all(isinstance(values, list) and all(isinstance(value, bytes) for value in values) \
            for values in blobs.values())

        try:
            self.internal_connection.add_s(str(base),
              [(name, [text.utf8_of(value) for value in attrs[name]]) for name in attrs
                if not seq.null(attrs[name])] +
              [(name, blobs[name]) for name in blobs
                if not seq.null(blobs[name])])
        except ldap.ALREADY_EXISTS:
            raise exceptions.Already_Exists(base)
        except ldap.OBJECT_CLASS_VIOLATION:
            raise exceptions.Object_Class_Violation(base)

    def retrieve(self, base, attrs = Ellipsis, blobs = []):

        result = list(self.query(base, attrs = attrs, blobs = blobs))
        return None if seq.null(result) else result[0][1]

    def change(self, base, attrs = {}, blobs = {}):

        assert isinstance(base, Dn)
        assert isinstance(attrs, dict) and all(isinstance(key, str) for key in attrs) \
          and all(isinstance(values, list) and all(isinstance(value, str) for value in values) \
            for values in attrs.values())
        assert isinstance(blobs, dict) and all(isinstance(key, str) for key in blobs) \
          and all(isinstance(values, list) and all(isinstance(value, bytes) for value in values) \
            for values in blobs.values())

        if seq.null(attrs) and seq.null(blobs):
            return

        try:
            self.internal_connection.modify_s(str(base),
              [(ldap.MOD_REPLACE, name, [text.utf8_of(value) for value in attrs[name]]) for name in attrs] +
              [(ldap.MOD_REPLACE, name, blobs[name]) for name in blobs])
        except ldap.NO_SUCH_OBJECT:
            raise exceptions.Not_Exists(base)
        except ldap.OBJECT_CLASS_VIOLATION:
            raise exceptions.Object_Class_Violation(base)

    def remove(self, base):

        assert isinstance(base, Dn)

        try:
            self.internal_connection.delete_s(str(base))
        except ldap.NO_SUCH_OBJECT:
            raise exceptions.Not_Exists(base)
        except ldap.NOT_ALLOWED_ON_NONLEAF:
            raise exceptions.Not_Empty(base)

    def remove_tree(self, base):

        assert isinstance(base, Dn)

        dns = [Dn.from_text(dn) for dn, _ in self.query_tree(base, attrs = [], blobs = [])]
        for dn in reversed(dns):
            self.remove(dn)
        return dns

    def diff_tree(self, base, blobs, wanted):

        existing = {Dn.from_text(raw_dn): data for raw_dn, data
          in self.query_tree(base, attrs = Ellipsis, blobs = blobs)}

        qualified_wanted = [(base.attach(relative_dn), attrs_blobs)
          for relative_dn, attrs_blobs in wanted]

        return diff_records(existing, qualified_wanted)

    def diff_level(self, base, filter, blobs, attr_for_dn, wanted_root, wanted):

        existing = {Dn.from_text(raw_dn): data for raw_dn, data
          in self.query_children(base, filter, attrs = Ellipsis,
          blobs = blobs)}
        some_attrs_blobs = self.retrieve(base, attrs = Ellipsis,
          blobs = blobs)
        if some_attrs_blobs is not None:
            existing[base] = some_attrs_blobs

        qualified_wanted = [(base, wanted_root)] + \
          [(base.extend(attr_for_dn, name, {}), attrs_blobs)
            for name, attrs_blobs in wanted]

        return diff_records(existing, qualified_wanted)


@contextmanager
def connection(conf, credential = None, *args, **kwargs):

    assert isinstance(conf, Config)
    assert credential is None or len(credential) == 2

    internal_connection = ldap.initialize(' '.join(conf.uris), *args, bytes_mode = False, **kwargs)
    if credential is not None:
        internal_connection.simple_bind_s(credential[0], credential[1])
    options = conf.raw_options()
    for opt in options:
        if opt in [ldap.OPT_URI, ldap.OPT_HOST_NAME]:
            continue
        safe_ldap_option_setting_using(internal_connection.set_option, opt, options[opt])

    conn = Ldap_Connection(conf.uris, internal_connection)

    try:
        yield conn
    finally:
        internal_connection.unbind()
