
'''
    Rich exceptions.
'''

# Copyright 2018 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Error', 'No_Connection',
  'Bad_Entry', 'Already_Exists', 'Not_Exists',
  'Object_Class_Violation']


class Error(Exception): pass


class No_Connection(Error):

    def __init__(self, uris, base):

        self.uris = uris
        self.base = base

    def __str__(self):

        return 'Unable to contact LDAP server(s) ' + ', '.join(self.uris) \
          + ', for base ' + str(self.base)


class Bad_Entry(Error):

    def __init__(self, dn):

        self.dn = dn


class Already_Exists(Bad_Entry):

    def __str__(self):

        return 'Entry already exists: {}'.format(str(self.dn))


class Not_Exists(Bad_Entry):

    def __str__(self):

        return 'Entry does not exist: {}'.format(str(self.dn))


class Not_Empty(Bad_Entry):

    def __str__(self):

        return 'Entry is not empty: {}'.format(str(self.dn))


class Object_Class_Violation(Bad_Entry):

    def __str__(self):

        return 'Object class violation: {}'.format(str(self.dn))
