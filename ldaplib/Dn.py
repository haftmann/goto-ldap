
'''
    Abstract data type for LDAP DNs
'''

# Copyright 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Dn']


import ldap.dn

from mopynaco import seq
from mopynaco import text


# In a multi-valued rdn, the primary key is chosen according to the following priority list
# This is an LHM pragmatism -- the LDAP standard does not define such a priority!
rdn_priority = ['cn', 'dc', 'uid', 'o', 'ou', 'c', 'st', 'l', 'street']

rdn_priority_doc = ', '.join(rdn_priority)
  ## only for documentation


def iter_fragment(fragment):

    (key, value, secondary) = fragment
    yield (key, value)
    for key_value in secondary.items():
        yield key_value


def implode(fragments):

    return ldap.dn.dn2str([[(key, value, 1)
      for key, value in iter_fragment(fragment)] for fragment in fragments])


def primary_key_of(d):

    for key in rdn_priority:
        if key in d:
            return key
    return min(d.keys())


def explode(txt):

    assert isinstance(txt, str)
    fragments = []
    for rdn in ldap.dn.str2dn(text.utf8_of(txt)):
        secondary = dict((key, value) for key, value, _ in rdn)
        key = primary_key_of(secondary)
        value = secondary.pop(key)
        fragments.append((key, value, secondary))
    return fragments


class Dn:

    def __init__(self, fragments):

        assert all(isinstance(key, str) and isinstance(value, str)
          for fragment in fragments for key, value in iter_fragment(fragment))
        self.fragments = fragments

    @classmethod
    def from_text(Class, txt):

        return Class(explode(txt))

    def __eq__(self, other):

        return str(self) == str(other)

    def __ne__(self, other):

        return str(self) != str(other)

    def __hash__(self):

        return hash(str(self))

    def extend(self, key, value, secondary = {}):

        return Dn([(key, value, secondary)] + self.fragments)

    def attach(self, dn):

        assert isinstance(dn, Dn)
        return Dn(dn.fragments + self.fragments)

    def __str__(self):

        return implode(self.fragments)

    def explode(self):

        return list(reversed(self.fragments))

    def order_key(self):

        chain = tuple(reversed([(key, value) for key, value, _ in self.fragments]))
        return len(chain), chain

    def common_prefix_suffixes(self, other):

        def orient(fragments):
            return [(key, value) for key, value, secondary in reversed(fragments)]
        zs, xs, ys = seq.common_prefix_suffixes(orient(self.fragments),
          orient(other.fragments))
        return Dn(self.fragments[-len(zs):]), list(reversed(xs)), list(reversed(ys))

    def __repr__(self):

        return 'Dn.from_text(' + repr(str(self)) + ')'
