
'''
    Really pythonic interfaces for python-ldap.
'''

# internal imports for documentation purpose only!
from . import Config as _config
from . import connection as _connection

# convenient aliasses
from .Config import Config
from .connection import connection
