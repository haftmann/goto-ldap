
'''
    Embedded domain-specific language for LDAP filters according to RFC 4515.
'''

# Copyright 2014 - 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['forany', 'forall', 'exists', 'equals', 'matches', 'approx',
  'greater_eq', 'less_eq', 'ext_attr', 'ext', 'anything', 'nothing']


import operator
import functools

import ldap.filter

from mopynaco import seq


class Abstract:

    def __str__(self):

        return None

    def __invert__(self):

        return Negation(self)

    def __and__(self, other):

        return Conjunction(self, other)

    def __or__(self, other):

        return Disjunction(self, other)

    def __repr__(self):

        return '<filter ' + str(self) + '>'


class Atomic(Abstract):

    def __init__(self, txt):

        self.txt = txt

    def __str__(self):

        return '(' + self.txt + ')'


class Negation(Abstract):

    def __init__(self, filter):

        self.filter = filter

    def __str__(self):

        return '(!' + str(self.filter) + ')'


class Compound(Abstract):

    op = None

    def __init__(self, *filters):

        self.filters = []
        for filter in filters:
            if isinstance(filter, type(self)):
                self.filters += filter.filters
            else:
                self.filters.append(filter)

    def __str__(self):

        return '(' + self.op + ''.join(map(str, self.filters)) + ')'


class Conjunction(Compound):

    op = '&'


class Disjunction(Compound):

    op = '|'


def is_attr(txt):

    return isinstance(txt, str) and ldap.filter.escape_filter_chars(txt) == txt


def exists(attr):

    assert is_attr(attr)
    return Atomic(attr + '=*')


def equals(attr, txt):

    assert is_attr(attr)
    assert isinstance(txt, str)
    return Atomic(attr + '=' + ldap.filter.escape_filter_chars(txt))


def matches(attr, txts):

    assert is_attr(attr)
    assert isinstance(txts, list) and all(isinstance(txt, str) for txt in txts)
    return Atomic(attr + '='
      + '*'.join([ldap.filter.escape_filter_chars(txt) for txt in txts]))


def approx(attr, txt):

    assert is_attr(attr)
    assert isinstance(txt, str)
    return Atomic(attr + '~=' + ldap.filter.escape_filter_chars(txt))


def greater_eq(attr, txt):

    assert is_attr(attr)
    assert isinstance(txt, str)
    return Atomic(attr + '>=' + ldap.filter.escape_filter_chars(txt))


def less_eq(attr, txt):

    assert is_attr(attr)
    assert isinstance(txt, str)
    return Atomic(attr + '<=' + ldap.filter.escape_filter_chars(txt))


def ext_attr(attr, dn, oid, txt):

    assert is_attr(attr)
    assert isinstance(dn, str)
    assert oid is None or is_attr(oid)
    assert isinstance(txt, str)
    return Atomic(attr + (':dn' if dn else '')
      + (':' + oid if oid else '')
      + ':=' + ldap.filter.escape_filter_chars(txt))


def ext(dn, oid, txt):

    assert is_attr(oid)
    assert isinstance(txt, str)
    return Atomic((':dn' if dn else '') + ':' + oid
      + ':=' + ldap.filter.escape_filter_chars(txt))


anything = exists('objectClass')


nothing = ~ anything


def forall(filters):

    xs = list(filters)
    return anything if seq.null(xs) \
      else functools.reduce(operator.__and__, xs)


def forany(filters):

    xs = list(filters)
    return nothing if seq.null(xs) \
      else functools.reduce(operator.__or__, xs)
