
'''
    LHM copy-on-write fundamentals.
'''

# Copyright 2017 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['attr_name', 'freeze_value', 'removed_value',
  'frozen_filter', 'is_frozen', 'is_removed']


import ldaplib.filter


attr_name = 'FAIstate'
freeze_value = 'freeze'
removed_value = '|removed'


frozen_filter = ldaplib.filter.equals(attr_name, freeze_value)


def is_frozen(entry):

    return any(txt.find(freeze_value) >= 0 for txt in entry.get(attr_name, []))


def is_removed(entry):

    return any(txt.find('removed') >= 0 for txt in entry.get(attr_name, []))
