
'''
    LHM POSIX users and groups.
'''

# Copyright 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['search_base_users', 'search_base_groups', 'get',
  'get_group_members', 'get_groups_of']


import ldaplib
import ldaplib.filter

import lhm.unit
import lhm.aux


def search_base_users(unit):

    return unit.search_base.attach(unit.organization.user_infix)


def search_base_groups(unit):

    return unit.search_base.attach(unit.organization.group_infix)


def get(conf, unit, username, attrs = Ellipsis, blobs = []):

    assert isinstance(username, str)

    return lhm.aux.invoke_ldap_get_object_for(conf, unit, '-user=' + username, attrs, blobs)


def get_group_members(conf, unit, groupname, *, filter = ldaplib.filter.anything):

    search_base = search_base_groups(unit).extend('cn', groupname)
    with ldaplib.connection(conf) as conn:
        for _, (entry, _) in conn.query(search_base,
          lhm.unit.filter(unit) & ldaplib.filter.exists('memberUid') &
          filter, ['memberUid']):
            return entry['memberUid']
    return []


def get_groups_of(conf, unit, username, *, filter = ldaplib.filter.anything):

    search_base = search_base_groups(unit)
    groupnames = set()
    with ldaplib.connection(conf) as conn:
        for _, (entry, _) in conn.query_children(search_base,
          lhm.unit.filter(unit) & ldaplib.filter.exists('cn') &
          ldaplib.filter.equals('memberUid', username) & filter, ['cn']):
            groupnames.add(entry['cn'][0])
    return groupnames
