
'''
    LHM administrative units.
'''

# Copyright 2014 - 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['get_all', 'from_file', 'from_system', 'create',
  'filter', 'obtain_finder', 'obtain_marker']


from mopynaco import seq
from mopynaco import text
from mopynaco import props
from mopynaco.Record import Record
from mopynaco import pathutil
from mopynaco import subproc

from ldaplib.Config import Config
from ldaplib.Config import loc_system_config as ldap_system_config
from ldaplib.Dn import Dn
import ldaplib.filter

import lhm.organization


unit_data = '/var/lib/goto-ldap/unit.properties'
alternative_ldap_config = '/etc/ldap/ldap-shell.conf'


class Unit(Record('search_base', 'tag', 'shortname',
  'organization')): pass


def some_elem(xs):

    return None if len(xs) == 0 else xs[0]


def checked_capture(*args, **kwargs):

    output, _, exit_value = subproc.capture(*args, **kwargs)
    if exit_value != 0:
        raise Exception('Bad exit value: {0}'.format(exit_value))
    return output


def get_all(org_conf = lhm.organization.lhm_conf):

    organization = lhm.organization.from_file(org_conf)

    output = checked_capture('/usr/bin/list-administrative-units', ['-c', org_conf])

    units = dict()
    for lines in seq.slices(output.split('\n'), 4):
        shortname = lines[0]
        search_base = Dn.from_text(lines[2][2:])
        tag = lines[3][2:]
        units[shortname] = Unit(search_base = search_base, tag = tag,
          shortname = shortname, organization = organization)
    return units


def from_file(loc = unit_data, organization = None):

    assert organization is None \
      or isinstance(organization, lhm.organization.Organization)

    if organization is None:
        organization = lhm.organization.from_system()

    _, _, unit_props = props.plain.read(unit_data)
    if not all(key in unit_props for key in ('ldap_base', 'tag', 'shortname')):
        raise Exception('Bad unit configuration: {0}'.format(loc))
    search_base = Dn.from_text(unit_props.lookup('ldap_base'))
    raw_tag = unit_props.lookup('tag')
    tag = raw_tag if raw_tag else None
    shortname = unit_props.lookup('shortname')

    return Unit(search_base = search_base, tag = tag,
      shortname = shortname, organization = organization)


def from_system():

    return from_file(unit_data)


def write_var(writer, var, value):

    writer.write('{0}={1}\n'.format(var, subproc.safe_shellarg(value)))


def create(uris, search_base, tag = None):

    assert isinstance(search_base, Dn)
    base_fragments = search_base.explode()
    assert len(base_fragments) > 0, 'Bad unit search base: {}'.format(search_base)
    assert base_fragments[-1][0] == 'ou', 'Bad unit search base: {}'.format(search_base)
    shortname = base_fragments[-1][1]

    raw, _, _ = props.plain.read(unit_data, strict = False)
    unit_props = props.Props() \
      .update('ldap_base', str(search_base)) \
      .update('tag', tag if tag is not None else '') \
      .update('shortname', shortname)
    with pathutil.atomicity_guard(unit_data) as emerging:
        props.plain.write(emerging, raw, unit_props)

    with pathutil.atomicity_guard(ldap_system_config) as emerging:
        with text.writing_utf8(emerging) as writer:
            writer.write('# THIS IS A GENERATED FILE. DO NOT EDIT!\n\n')
            writer.write('URI   {0}\n'.format(' '.join(uris)))
            writer.write('BASE  {0}\n'.format(str(search_base)))

    with pathutil.atomicity_guard(alternative_ldap_config) as emerging:
        with text.writing_utf8(emerging) as writer:
            writer.write('# THIS IS A GENERATED FILE. DO NOT EDIT!\n\n')
            write_var(writer, 'LDAP_URIS', ' '.join(uris))
            write_var(writer, 'LDAP_BASE', str(search_base))
            write_var(writer, 'UNIT_TAG', tag if tag is not None else '')
            write_var(writer, 'UNIT_TAG_FILTER',
              str(ldaplib.filter.equals('gosaUnitTag', tag)
                if tag is not None else ldaplib.filter.exists('objectClass')))
            write_var(writer, 'SHORTNAME', shortname)


def filter(unit):

    return ldaplib.filter.equals('gosaUnitTag', unit.tag) \
      if unit.tag is not None else ldaplib.filter.anything


def pick(loc_organization = None, shortname = None):

    if shortname is not None:
        org = lhm.organization.from_system() if loc_organization is None \
          else lhm.organization.from_file(loc_organization)
        conf = Config.from_system().update(uris = org.uris)
        units = get_all()
        if shortname not in units:
            raise KeyError('No such shortname: {}'.format(shortname))
        unit = units[shortname]
    else:
        conf = Config.from_system()
        unit = from_system()

    return conf, unit


def obtain_finder():

    units = get_all()
    search_bases = sorted([(units[shortname].search_base, units[shortname])
      for shortname in units],
      key = lambda search_base_unit: len(search_base_unit[0].explode()),
      reverse = True)

    def unit_of_dn(dn):
        for search_base, unit in search_bases:
            if search_base.common_prefix_suffixes(dn)[0] == search_base:
                return unit
        return None

    return unit_of_dn


def obtain_marker(unit):

    if unit.tag is None:
        return dict
    else:
        def marked_dict(**attrs):
            attrs_new = dict(attrs)
            attrs_new['objectClass'] = attrs_new.get('objectClass', []) + ['gosaAdministrativeUnitTag']
            attrs_new['gosaUnitTag'] = [unit.tag]
            return attrs_new
        return marked_dict
