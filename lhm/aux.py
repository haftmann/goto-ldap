
'''
    Specifics of the LHM directory, including implicit group resolution.
'''

# Copyright 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['invoke_ldap_get_object_for']


import re
import os

from mopynaco import subproc

import ldaplib.ldif
import lhm.unit


attr_dn = 'dn'


def invoke_ldap_get_object(conf, unit, query1, query2,
  is_binary_attribute = lambda _: False, timeout = None, query3 = None):

    assert len(conf.uris) > 0

    actual_timeout = conf.timeout or timeout

    cmd = 'ldap_get_object'
    args = ['-format=[a:v]', '-casefold=none']
    if unit is not None:
        args.append('-base=' + str(unit.search_base))
        args.append('-filter=' + str(lhm.unit.filter(unit)))
    if actual_timeout is not None:
        args.append('-timeout=' + str(actual_timeout))
    args.append(query1)
    args.append(query2)
    if query3 is not None:
        args.append(query3)

    env = dict(os.environ)
    env['LDAPNOINIT'] = 'true'

    for uri in conf.uris:
        object_txt, error_txt, exit_value = subproc.capture('ldap_get_object', ['-host=' + uri] + args, env = env)
        if exit_value == 0:
            break

    if exit_value != 0:
        raise EnvironmentError('Unable to execute ldap query:\n'
          + ' '.join([subproc.safe_shellarg(arg) for arg in [cmd] + args])
          + '\nyields\n'
          + error_txt)

    # ldif.py from python-ldap version 2.4.41-1 needs at least 3 lines
    # otherwise an "EOFError: EOF reached after 2 lines" occurs
    object_txt += "\n\n"

    return ldaplib.ldif.internalize(object_txt, is_binary_attribute)


def invoke_ldap_get_object_for(conf, unit, query, attrs = Ellipsis, blobs = [], timeout = None):

    assert attrs is Ellipsis or all(isinstance(attr, str) for attr in attrs)
    assert blobs is Ellipsis or all(isinstance(attr, str) for attr in blobs)
    assert blobs is not Ellipsis or attrs is Ellipsis

    both_attrs = list(attrs) if attrs is not Ellipsis else Ellipsis
    if both_attrs is not Ellipsis:
        if attr_dn not in attrs:
            both_attrs.append(attr_dn)
        if blobs is not Ellipsis:
            for attr in blobs:
                if attr not in both_attrs:
                    both_attrs.append(attr)

    attr_matcher = '@(' + '|'.join(re.escape(attr) for attr in both_attrs) + ')' \
      if both_attrs is not Ellipsis else '@.*'
    def is_binary_attribute(attr):
        return blobs is Ellipsis or attr in blobs

    result, result_blob = invoke_ldap_get_object(conf, unit,
      query, attr_matcher, is_binary_attribute, timeout = None)

    if attrs is not Ellipsis:
        raw_attr_mapping = {attr.lower(): attr for attr in result}
        for attr in attrs:
            result.setdefault(raw_attr_mapping.get(attr.lower(), attr), [])
    if blobs is not Ellipsis:
        for attr in blobs:
            result_blob.setdefault(raw_attr_mapping.get(attr.lower(), attr), [])

    return result, result_blob
