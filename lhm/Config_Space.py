
'''
    Configuration spaces: mappings of relative file names
    to content, text or binary.
'''

# Copyright 2016 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Config_Space']


import functools
from os import path
import os
import stat

from mopynaco import seq
from mopynaco.Sum import Sum
from mopynaco import text
from mopynaco import pathutil


class Content(Sum('text', 'blob')): pass


class Config_Space:

    def __init__(self):

        self.content_mapping = {}

    def prepare_write(self, loc):

        assert all(isinstance(fragment, str) for fragment in loc)

        accessor = tuple(loc)

        assert pathutil.explode(pathutil.implode(loc)) == accessor, \
          'Bad component in path: {}'.format(accessor)

        new = Config_Space()
        new.content_mapping = dict(self.content_mapping)
        return new, accessor

    def augment_text(self, loc, lines):

        assert all(isinstance(line, str) for line in lines)

        new, accessor = self.prepare_write(loc)
        existing = new.content_mapping.get(accessor, Content(text = []))
        if existing.text is None:
            raise KeyError('Attempt to overwrite binary blob by text lines for entry »{}«'
              .format(pathutil.implode(loc)))
        new.content_mapping[accessor] = Content(text = existing.text + lines)
        return new

    def augment_nonempty_text(self, loc, lines):

        assert all(isinstance(line, str) for line in lines)

        return self if seq.null(lines) else self.augment_text(loc, lines)

    def place_blob(self, loc, blob, executable = False):

        assert isinstance(blob, bytes)

        new, accessor = self.prepare_write(loc)
        if accessor in new.content_mapping:
            raise KeyError('Attempt to overwrite entry »{}« by binary blob'
              .format(pathutil.implode(loc)))
        new.content_mapping[accessor] = Content(blob = (blob, executable))
        return new

    def merge(self, other):

        new = self
        for loc, content in other.content_mapping.items():
            if content.text is not None:
                new = new.augment_text(loc, content.text)
            else:
                new = new.place_blob(loc, content.blob[0],
                  executable = content.blob[1])
        return new

    def serialize(self):

        for loc in sorted(self.content_mapping):
            content = self.content_mapping[loc]
            yield (pathutil.implode(loc),
              (text.utf8_of('\n'.join(content.text + [''])), False)
                if content.text is not None
                else content.blob)

    def dump(self, base, descend_prefix = False):

        if descend_prefix:
            prefix = self.common_prefix()
            base_dir = path.join(base, prefix) if prefix else base
        else:
            base_dir = base

        if path.isdir(base_dir):
            if not seq.null(os.listdir(base_dir)):
                raise IOError('Directory not empty: {}'.format(base_dir))
        elif path.exists(base_dir):
            raise IOError('File exists: {}'.format(base_dir))
        else:
            os.mkdir(base_dir)

        for loc, (content, executable) in self.serialize():
            dst = path.join(base, loc)
            dst_loc = path.dirname(dst)
            if not path.isdir(dst_loc):
                os.makedirs(dst_loc)
            with open(dst, 'wb') as writer:
                writer.write(content)
            if executable:
                os.chmod(dst, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR
                  | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH)

    def common_prefix(self):

        return None if seq.null(self.content_mapping) \
          else pathutil.implode(
            functools.reduce((lambda x, y: seq.common_prefix_suffixes(x, y)[0]),
            (prefix[:-1] for prefix in self.content_mapping)))
