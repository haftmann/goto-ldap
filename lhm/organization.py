
'''
    LHM organization as a whole.
'''

# Copyright 2014 - 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


from mopynaco.Record import Record

from ldaplib.Config import parse_raw
from ldaplib.Dn import Dn


__all__ = ['from_file', 'from_system', 'Organization']


lhm_conf = '/etc/goto-ldap/lhm.conf'


class Organization(Record('uris', 'search_base', 'fai_infix', 'gosa_infix', 'app_infix',
  'acl_infix', 'user_infix', 'user_primary_attribute',
  'system_infix', 'server_infix', 'workstation_infix', 'printer_infix', 'terminal_infix',
  'device_infix', 'group_infix', 'incoming_infix',
  'organization_wide_server_base',
  'lhm_object_path_base')): pass


def from_file(loc):

    raw = parse_raw(loc)
    return Organization(uris = raw['URI'].split(),
      search_base = Dn.from_text(raw['BASE']),
      fai_infix = Dn.from_text(raw['FAI_INFIX']),
      gosa_infix = Dn.from_text(raw['GOSA_INFIX']),
      app_infix = Dn.from_text(raw['APP_INFIX']),
      acl_infix = Dn.from_text(raw['ACL_INFIX']),
      user_infix = Dn.from_text(raw['USER_INFIX']),
      user_primary_attribute = raw['USER_PRIMARY_ATTRIBUTE'],
      system_infix = Dn.from_text(raw['SYSTEM_INFIX']),
      server_infix = Dn.from_text(raw['SERVER_INFIX']),
      workstation_infix = Dn.from_text(raw['WORKSTATION_INFIX']),
      printer_infix = Dn.from_text(raw['PRINTER_INFIX']),
      terminal_infix = Dn.from_text(raw['TERMINAL_INFIX']),
      device_infix = Dn.from_text(raw['DEVICE_INFIX']),
      group_infix = Dn.from_text(raw['GROUP_INFIX']),
      incoming_infix = Dn.from_text(raw['INCOMING_INFIX']),
      organization_wide_server_base = Dn.from_text(raw['ORGANIZATION_WIDE_SERVER_BASE']),
      lhm_object_path_base = Dn.from_text(raw['LHM_OBJECT_PATH_BASE']))


def from_system():

    return from_file(lhm_conf)
