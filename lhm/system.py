
'''
    LHM systems (servers, workstations, printers, etc.).
'''

# Copyright 2015 - 2017 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['get_by_hostname', 'get_by_sid_mac', 'split_host_name']


import lhm.aux


def get(conf, unit, object_class, ident, attrs = Ellipsis, blobs = []):

    assert isinstance(object_class, str)
    assert '/' not in object_class
    assert isinstance(ident, str)

    return lhm.aux.invoke_ldap_get_object_for(conf, unit, '-object=' + object_class + '/' + ident,
      attrs, blobs)


def get_by_hostname(conf, unit, hostname, attrs = Ellipsis, blobs = []):

    return get(conf, unit, 'goHard', hostname, attrs, blobs)


def get_by_sid_mac(conf, unit, sid_mac, attrs = Ellipsis, blobs = []):

    return get(conf, unit, 'goHard', sid_mac, attrs, blobs)


def split_host_name(fully_qualified_host_name):

    return fully_qualified_host_name.split('.', 1) \
      if '.' in fully_qualified_host_name \
      else (fully_qualified_host_name, None)
