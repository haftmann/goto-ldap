
'''
    LHM releases.
'''

# Copyright 2014 - 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['split', 'join', 'attach', 'filename_of', 'of_filename']


from mopynaco import text

from ldaplib.Dn import Dn


sep = '/'
subst_for_underscore = '·'


def is_sane(s):

    return isinstance(s, str) and '\0' not in s and subst_for_underscore not in s


def split(name):

    assert is_sane(name), 'Bad release identifier: {}'.format(name)
    fragments = name.split(sep)
    assert '' not in fragments, 'Bad release identifier: {}'.format(name)
    return fragments


def join(fragments):

    assert all(is_sane(fragment) for fragment in fragments)
    assert all(sep not in fragment for fragment in fragments)
    return sep.join(fragments)


def attach(dn, name):

    assert isinstance(dn, Dn)
    base = dn
    fragments = split(name)
    for fragment in fragments:
        base = base.extend('ou', fragment)
    return base


def filename_of(name):

    return text.filename_of('_'.join(txt.replace('_', subst_for_underscore)
      for txt in split(name)))


def of_filename(name):

    return join(list(txt.replace(subst_for_underscore, '_')
      for txt in text.of_filename(name).split('_')))
