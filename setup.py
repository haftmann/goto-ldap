
from distutils.core import setup

from debian.changelog import Changelog

with open('debian/changelog', 'rb') as reader:
    chlog = Changelog(reader, max_blocks = 1)
version = chlog.get_version().full_version

description = 'LDAP interface and fundamental LHM infrastructure data access'
long_description = '''Python LDAP access layer and interfaces to fundamental
LHM infrastructure data.'''


settings = dict(

    name = 'goto-ldap',
    version = version,

    packages = ['ldaplib', 'lhm'],

    author = 'Florian Haftmann',
    author_email = 'florian.haftmann@muenchen.de',
    description = description,
    long_description = long_description,

    license = 'EUPL and GPL',
    url = 'http://www.muenchen.de'

)

setup(**settings)
