LDAP_GET_OBJECT(1)
==================


NAME
----
ldap_get_object - Extract LDAP data with support for attribute inheritance from posix and object groups


SYNOPSIS
--------
*ldap_get_object* ['OPTIONS'] *-user=*'uid' ['attributeRegex' '...']

*ldap_get_object* ['OPTIONS'] *-object=*'objectClass'/'cn'|'ou'|'macaddress' ['attributeRegex' '...']

*ldap_get_object* [*-host=*'ldapserver'] [*-base=*'dn'|'ou']
 [*-global*] [*-debug*] [*-multi*] [*-nowild*] [*-timeout=*'seconds']
 [*-enctrigger=*'regex'|none] [**-format=**[a:v]|a:v|A=v|a=v|A=(v)|a=(v)|v]
 [**-sort=**alpha|precedence]
 [**-casefold=**lower|upper|none]
 [*-filter=*'filter']
 *-user=*'...'|*-object=*'...' ['attributeRegex' '...']

*Note:* You may use the * wildcard within 'cn' and 'ou'. This works even with *-base*.

DESCRIPTION
-----------
Searches the LDAP directory for information about a user or other object (group, server, workstation, printer,...) and prints the
selected attributes. *ldap_get_object* automatically searches for posix groups or object groups
the object is a member of and includes inherited attributes in its output. The *-debug* switch can be used to find out from which
group or object the attributes are coming.

Each 'attributeRegex' is a regular expression that selects all attributes with
matching names for output. The first character of 'attributeRegex' determines what
happens if different nodes (e.g. the user's posixAccount and a posixGroup) provide an attribute 
with the same name. If 'attributeRegex' starts with "@", then the result will include all values.
If 'attributeRegex' does NOT start with "@", then an attribute from the user's
posixAccount node beats a posix group, which beats an object group that includes the user which 
beats an object group that contains a posix group.

Use "@" when multiple values make sense for an attribute (e.g. "@gotoShare") or to get complete information
for doing diagnostics.

If 2 sources with the same precedence (e.g. 2 posix groups) provide an attribute
of the same name, selected by an 'attributeRegex' that doesn't start with "@",
a WARNING is signalled and the program picks one of the conflicting
attributes.

If no 'attributeRegex' is specified, it will default to "@.*" and all attributes are printed.

An __attributeRegex__ is always matched against the complete attribute name, i.e.
an 'attributeRegex' "name" will NOT match an attribute "surname".
Matching is always performed case-insensitive.

EXAMPLES
--------
`ldap_get_object -user=felix.wollmux -debug`::
  Get all attributes of felix.wollmux, including those inherited from groups. Print information for individual groups.
  felix.wollmux must be located in the same administrative unit as the caller.

`ldap_get_object -global -user=felix.wollmux @cn`::
  Print the names of all (posix and object) groups felix.wollmux is a member of. Because of *-global*, felix.wollmux
  will be found, even if he is in a different administrative unit.
  
`ldap_get_object -object=goHard/fresenius*`::
  Print attributes of the computer fresenius.afd.dir.muenchen.de ("afd.dir.muenchen.de" is matched by *). This
  includes attributes inherited from object groups fresenius is a member of.

`ldap_get_object -object=*/00:1a:4b:b1:a8:46`::
  Print attributes of the computer with MAC address 00:1a:4b:b1:a8:46. The object class is specified as * which matches
  any objectClass. Note: In shell scripts you should avoid * because it may slow down the query.

`ldap_get_object -global -base=Personal* -object=posixGroup/camera`::
  Print all attributes (including members) of the group "camera" in the administrative unit "Personal- und Organisationsreferat".

`ldap_get_object -user=felix.wollmux gotoprofile.*`::
  Print all attributes related to the profile sync for user felix.wollmux.

`ldap_get_object -user=felix.wollmux @gotoprofile.*`::
  Print all attributes related to the profile sync for user felix.wollmux 'without' resolving precedence (note the "@" character). If felix.wollmux
  gets different attribute values from different sources (e.g. he's a member of conflicting groups), the output will
  contain multiple lines with different values for the same attribute. This can be used to quickly check for issues before
  examining closer with *-debug*.

`ldap_get_object -global -base=Personal* -object=posixGroup/* -multi -format=v @cn`::
  Print out the names (without preceding "cn: ") 
  of all posix groups of the administrative unit "Personal- und Organisationsreferat".
  
`eval "$(ldap_get_object -user=felix.wollmux -format=A=v gotokioskprofile )"`::
  Create a shell variable GOTOKIOSKPROFILE that contains the kiosk profile URL of user felix.wollmux.

`eval "$(ldap_get_object -user=felix.wollmux -format=A=v samba.* )"`::
  Import all samba* attributes of user felix.wollmux as shell variables (e.g. SAMBABADPASSWORDCOUNT, SAMBABADPASSWORDTIME). This
  is more efficient than using multiple eval statements.


OPTIONS
-------
*-user=*'uid'::
  Selects the posixAccount object that matches the given 'uid' and the posixGroup objects whose memberuid matches 'uid'. 
  'uid' may include * to match arbitrary substrings. *-user* implies *-multi*.
  Examples: *-user=*'hans.mustermann', *-user=*'hans.m*mann'
  
*-object=*'objectClass'/'cn'|'ou'|'macaddress'::
  Selects an object by matching the 'objectClass' and 'cn', 'ou' or 'macaddress'. 
  'cn' and 'ou' may include * to match arbitrary substrings. You may use * instead of 'objectClass' to match
  all classes (but that may slow down the query).
  Useful object classes are:
- *goHard*: servers and workstations
- *goServer* (NOT gotoServer!): servers
- *gotoWorkstation*: workstations
- *gotoPrinter*: printers
- *posixAccount*: user
- *posixGroup*: posix group
- *gosaGroupOfNames*: object group
  
  Examples: :::
  *-object=*'goServer/vts.ref.muenchen.de', *-object=*'\*/client0815', *-object=*'posixGroup/startmenugroup',
  *-object=*'goHard/00:1e:2f:dc:40:ff'

*-debug*::
  In addition to the final result, all contributing objects (i.e. the object itself and posix and object groups) will
  be printed individually, so that you can see where the attributes of the final result come from.

*-host=*'ldapserver'::
  Specify the LDAP server to query (with or without "ldap://"). If unset, the server will be taken from '/etc/ldap/ldap.conf'.

*-base=*'dn'|'ou'::
  Specify the search base. If unset, it will be read from '/etc/ldap/ldap.conf'.
  If the parameter contains "=" it is taken to be the complete 'dn' of the base object.
  Otherwise the base is an object with objectClass gosaAdministrativeUnit whose 'ou' matches
  the parameter. In this case you may use * to match an arbitrary substring.
  Examples: :::
  - *-base=*'ou=Direktorium,o=Landeshauptstadt_München,c=de'
  - *-base=*'D*um'
  
*-global*::
  This is a shortcut for *-host=*'(default organizational host)' *-base=*'c=de'. If *-host* and/or *-base* are used, they will override this.

*-nowild*::
  Causes "*" to be interpreted as literal asterisk and not as a wildcard.

*-multi*::
  Multiple objects matching the query are merged. Without *-multi* a query that matches multiple objects
  causes an error. Note: For *-user* queries *-multi* is always active (for technical reasons).

*-timeout=*'seconds'::
  All LDAP requests will time out after this many 'seconds'. Note that this does not mean that *ldap_get_object* will finish
  within this time limit, because several LDAP requests may be involved. Default timeout is 10s.

*-filter=*'filter'::
 The LDAP-Expression 'filter' will be ANDed with all searches done by this program. Use this to select by gosaUnitTag. 
 Example: *-filter=*'(gosaUnitTag=1154342234048479900)'

*-enctrigger=*'regex'|none::
  All dn and attribute values will be tested against 'regex'. Whenever a value matches 'regex' it will be output
  base64-encoded. Matching is performed case-sensitive and unless ^ and $ are used in 'regex', matching
  substrings are enough to trigger encoding. The default is "[\x00-\x1f]" which causes base64-encoding only
  for values that contain control characters. +
  **-enctrigger=**none disables base64 encoding completely.

**-format=**[a:v]|a:v|A=v|a=v|A=(v)|a=(v)|v::
  Selects the output format. The following choices exist:
- *[a:v]*: standard LDIF format, i.e. attribute name followed by colon (double-colon if base64-encoded) followed by value.
- *a:v*: standard LDIF format, i.e. attribute name followed by colon (double-colon if base64-encoded) followed by value; values lexicographically
sorted with no duplicates.  Default (for historic reasons).
- *A=v*: assignment statements appropriate for being sourced by a POSIX shell, with UPPERcase variable names. Implies **-casefold**=upper.
- *a=v*: assignment statements appropriate for being sourced by a POSIX shell, with lowercase variable names. Implies **-casefold**=lower.
- *A=(v)*: assignment statements appropriate for being sourced by a BASH shell, with UPPERcase array variable names and decreasing precedence. Implies **-casefold**=upper.
- *a=(v)*: assignment statements appropriate for being sourced by a BASH shell, with lowercase array variable names and decreasing precedence. Implies **-casefold**=lower.
- *v*: attribute values only.

**-sort=**alpha|precedence::
  Select the order in which attributes are printed. **-sort=**alpha (default) prints in alphabetical order of
  attribute names. **-sort=**precedence prints attribute values from sources with lower 
  precedence before those with higher precedence. Attributes with the same precedence are printed in alphabetical order. +
  Notes:
- The dn is always printed first (if it is selected by 'attributeRegex').
- **-sort=**precedence only affects attributes selected with "@" in their 'attributeRegex', because for other attributes only
  the value with the highest precedence is printed.
- **-sort=**precedence may cause duplicate lines in the output if the same attribute value is provided by multiple sources with
  different precedence.

**-casefold=**lower|upper|none::
  Affects the attribute names printed for **-format=**a:v and **-format=**[a:v]:
- **-casefold=**lower: (default) prints attribute names in lowercase.
- **-casefold=**upper: prints attribute names in UPPERcase.
- **-casefold=**none: keeps attributes as they are returned by the LDAP server.


CONF FILES
----------
'/etc/ldap/ldap.conf'::
  Provides the default values for **-host** and **-base**. +
  **Note:** A unittag tag filter is 'not' set by default. Use **-filter** if you need it.

AUTHOR
------
Matthias S. Benkmann, <matthias.benkmann@muenchen.de>


SEE ALSO
--------
The documentation of the underlying Perl function can be obtained through the command

  perldoc GOsa::LDAP

Related manpages:
  
**ldapsearch**(1), **ldapadd**(1), **ldapmodify**(1), **Net::LDAP**(3pm)
