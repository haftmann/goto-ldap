
# Zweck

Das Paket *@{control Source}* ist eine fundamentale Abstraktionsschicht über das
LDAP der LHM und zerfällt in zwei Teile:

* pragmatische generisch technische Abstraktion (Modul *@{identname
  ldaplib}*);

* fachliche LHM-spezifische Abstraktion über das LDAP (rudimentäre
  Anwendungslogik, Modul *@{identname lhm}*).

Diese Dokumentation soll die Grundlagen für ein prinzipielles
Verständnis dieser Module erklären; für Details ist Studium der Sourcen
und Experimentieren notwendig.


# Zur Historie

Das LDAP der LHM und der Basisclient haben eine lange gemeinsame
Historie; in dieser Zeit ist eine Unmenge an Material in
unterschiedlichsten Schattierungen entstanden, um die Kommunikation der
beiden zu ermöglichen oder zu vereinfachen.

Dieses Paket ist der Versuch, mittelfristig zu einer sauberen und
robusten technologischen Vereinheitlichung zu kommen und Insellösungen
abzulösen. Dabei wurde großer Wert darauf gelegt, nach außen hin ein
einheitliches Interface anzubieten. Intern gibt es hierbei abenteuerlich
anmutende Lösungen (z. B. Delegation an externe Prozesse), die in erster
Linie deshalb vorhanden sind, um Redundanzen zu vermeiden und bereits
existierendes Material weiterverwenden zu können. Langfristig ist es
wünschenswert, auch diese Bestandteile ablösen zu können.

Aus ähnlichen Gründen hat *@{control Source}* auch einen stillen Trabanten, das
Paket *libgosa-perl*.

Diese Situation ist auch der technische Grund dafür, dass zwei Dinge,
die eigentlich zwei getrennte Pakete ausmachen könnten (bzw. auf andere
fachliche Pakete verteilt werden könnten), hier in *einem* Paket
verteilt auf zwei Module zu finden sind.


# Technische Abstraktion

## Pragmatische Rahmenbedingungen

* In der Ausgangssituation lag für LDAP-Anfragen in Python 2 das Paket
  *python-ldap* vor. Wie viele Wrapper um C-Bibliotheken krankt es
  daran, dass es zwar die Funktionalität der Bibliothek zugänglich
  macht, aber nicht idiomatisch in die Sprache einbettet. Dieser
  Mangel wird in *@{identname ldaplib}* gemildert, insbesondere für lesende
  LDAP-Anfragen.

* Ein Blick in die Manpage von *ldap.conf* zeigt, dass es zahlreiche
  Möglichkeiten gibt, die LDAP-Konfiguration zu manipulieren. In
  *@{identname ldaplib}* wird mit etlicher Anstrengung peinlich darauf geachtet,
  nur Einstellungen aus dem Systembereich anzuwenden, nicht aus dem
  Benutzerbereich.

* Als Kodierung für Textdaten wird einheitlich *UTF-8* verwendet; der
  Benutzer des Pakets muss sich um Kodierung keine Gedanken machen.


## Elemente der Anfragesprache

Es ist etablierte Gewohnheit, LDAP-Anfragen textuell zu formulieren. Das
bringt die typischen Gefahren wie bei allem formalen Text, der einfach aus
kleineren Fragmenten konkateniert wird: Fehlerquellen und
Sicherheitslücken durch nicht korrekt gehandhabte Fragmente.

Aus diesem Grund verwendet *@{identname ldaplib}* eigene abstrakte Typen, um
Distinguished Names (DNs) und Filterausdrücke darzustellen. Damit kann
rohes Hantieren mit Textfragmenten vermieden werden. Diese abstrakten
Typen implementieren alle eine Konversion nach Text, so dass mit
*@{identname str}* die formale Textdarstellung des betreffenden
Werts ausgegeben und inspiziert werden kann.


### DNs und RDNs

DNs werden von Klasse *@{identname Dn in ldaplib.Dn}* in Modul
*@{identname ldaplib.Dn}* implementiert. Dabei besteht ein DN aus einer
Liste von RDNs. Ein RDN wiederum ist ein Schlüssel-Wert-Paar mit
optionalen sekundären Attributen. Diese »sekundären Attribute« sind eine
pragmatische Abbildung von mehrwertigen RDNs, die innerhalb der LHM
schon gesichtet worden sind; ist ein RDN mehrwertig, ist einer seiner
Schlüssel der Hauptschlüssel, nämlich derjenige, der in der Liste
*@{identval ldaplib.Dn.rdn_priority_doc}* am weitesten vorne steht. Die
anderen Schlüssel-Wert-Paare werden als sekundäre Attribute betrachtet.
Damit kann formal mit mehrwertigen RDNs gerechnet, z. B. gemeinsame
Präfixe bestimmt werden, ohne dass das Datenmodell die volle Komplexität
von mehrwertigen DNs abbilden muss. DNs sind hashbare Werte und können
auf (Un-)Gleichheit überprüft werden.

*@{function Dn in ldaplib.Dn}*

:   Konstruktion eines DNs aus einer Liste von RDNs; jeder RDN ist ein
    Tripel bestehend aus Hauptschlüssel, Hauptwert und einem Dictionary
    der sekundären Attribute.

*@{function Dn.from_text in ldaplib.Dn}*

:   Konstruktion eines DNs aus einer textuellen Darstellung.

*@{function Dn.explode in ldaplib.Dn}*

:   Liste der RNDs eines DNs, beginnend mit der Wurzel.

*@{function Dn.\_\_str\_\_ in ldaplib.Dn}*

:   Textuelle Darstellung eines DNs.

*@{function Dn.extend in ldaplib.Dn}*

:   Ergänzung eines DNs um einen weiteren RDN.

*@{function Dn.attach in ldaplib.Dn}*

:   Ergänzung eines DNs um die RDNs eines weiteren DNs.

*@{function Dn.order_key in ldaplib.Dn}*

:   Schlüssel, der Eltern topologisch vor ihre Kinder sortiert.

*@{function Dn.common_prefix_suffixes in ldaplib.Dn}*

:   Das gemeinsame Präfix und die verbleibenden Suffixe zweier DNs;
    sekundäre Attribute werden bei der Bestimmung ignoriert.


### Filter

Filterausdrücke werden aus atomaren Filtern in Modul *@{identname
ldaplib.filter}* zusammengesetzt; typische atomare Filter sind
*@{function exists in ldaplib.filter}*, *@{function equals in
ldaplib.filter}*, *@{function matches in ldaplib.filter}*, *@{function
approx in ldaplib.filter}*, *@{function greater_eq in ldaplib.filter}*
und *@{function less_eq in ldaplib.filter}*.

Man beachte insbesondere, dass ein »\*« in *@{function equals in
ldaplib.filter}* *immer* als literales Zeichen interpretiert wird. Falls
ein Suchmuster gewünscht ist, muss *@{function matches in
ldaplib.filter}* verwendet werden; das Argument stellt eine Liste von
Textfragmenten dar, zwischen denen beliebige Zeichen auftreten dürfen
(Wildcard »\*«).

Konjunktion (»und«), Disjunktion (»oder«) und Negation (»nicht«) von
Filtern wird über die in Python eingebauten (!) Operatoren *&*, *|* und
*\~* ausgedrückt.

Filterausdrücke in *@{identname ldaplib}* stellen somit eine Embedded
Domain-specific Language (EDSL) dar. Insbesondere können parametrisierte
Filter als Funktionen geschrieben werden.


## Konfiguration

Eine Herausforderung ist, dass per Default die LDAP-Konfiguration
vielfältig aus dem Benutzerbereich beeinflusst werden kann. Um das Chaos
perfekt zu machen, haben die Namen der Einstellungen in
Konfigurationsdateien und die betreffenden Optionen zur Laufzeit nichts
miteinander zu tun.

Der Lösungsansatz von *@{identname ldaplib}* ist folgender:

* Beim Initialisieren des Moduls *@{identname ldaplib}* werden die
  Default-Einstellungen der lokalen LDAP-Bibliothek explizit
  abgegriffen, *ohne* auf Konfigurationsdateien oder
  Umgebungsvariablen Rücksicht zu nehmen. Diese Einstellungen werden
  bei LDAP-Verbindungen immer explizit erzwungen, ganz gleich, welche
  dynamischen Einstellungen zum jeweiligen Zeitpunkt vorliegen.

* Der Typ *@{identname Config in ldaplib}* in Modul *@{identname
  ldaplib}* beschreibt einen expliziten Satz an Einstellungen, die
  konfigurierbar sind. Diese haben Priorität vor den
  Default-Einstellungen.

* Ein passender Wert vom Typ *@{identname Config in ldaplib}* kann auf
  folgende Weisen erstellt werden:

    * *@{function Config.from_system in ldaplib}* liest die
      Einstellungen aus der lokalen systemweiten Konfigurationsdatei;

    * *@{function Config.from_file in ldaplib}* liest die
      Einstellungen aus einer beliebigen Konfigurationsdatei;

    * *@{function Config in ldaplib}* übernimmt explizit vorgegebene
      Einstellungen;

    * *@{function Config.update in ldaplib}* erlaubt das Modifizieren
      von Einstellungen.

* Falls Einstellungen in *@{identname Config in ldaplib}* nicht
  explizit gesetzt werden, greifen Standardvorgaben, u.a. ein Timeout
  von *@{identval ldaplib.\_config.default\_timeout}* Sekunden.

* Soll der Satz an konfigurierbaren Einstellungen erweitert werden,
  geschieht dies in Modul *@{identname ldaplib}*. Dabei müssen
  geeignete Meta-Daten der Einstellung (wie lautet das Schlüsselwort
  in einer Konfigurationsdatei, wie wird sie geparst, wie wird sie
  externalisiert, was ist die Standardvorgabe etc.) definiert werden.


## Verbindungen

LDAP-Verbindungen werden mittels des Kontextmanagers *@{function
connection in ldaplib.\_connection}* in Modul *@{identname ldaplib}*
aufgebaut. Der Kontextmanager liefert einen Wert vom Typ *@{identname
Ldap_Connection in ldaplib.\_connection}*.

Auf Werten dieses Typs können konkrete Anfragen operieren.


## Einzeldatensatz-Anfragen

Einzeldatensatz-Anfragen operieren immer auf einem einzelnen Datensatz –
abgesehen vom formalen Sonderfall *@{function Ldap_Connection.remove_tree in ldaplib.\_connection}*.
Mit Einzeldatensatz-Anfragen können Datensätze auch angelegt, verändert
und gelöscht werden.

*@{function Ldap_Connection.create in ldaplib.\_connection}*

:   Anlegen eines neuen Eintrags.

    *@{argument base of ldaplib.\_connection.Ldap_Connection.create}*

    :   Basis des zu erstellenden Eintrags vom Typ *@{identname Dn in
        ldaplib.Dn}*.

    *@{argument attrs of ldaplib.\_connection.Ldap_Connection.create}*

    :   Zu setzende Textattribute; ein Dictionary von Schlüsseln
        zu Listen von Textwerten.

    *@{argument blobs of ldaplib.\_connection.Ldap_Connection.create}*

    :   Zu setzende Binärattribute; ein Dictionary von Schlüsseln
        zu Listen von Binärwerten.

*@{function Ldap_Connection.retrieve in ldaplib.\_connection}*

:   Abfragen eines existierenden Eintrags.

    *@{argument base of ldaplib.\_connection.Ldap_Connection.retrieve}*

    :   Basis des zu erstellenden Eintrags vom Typ *@{identname Dn in
        ldaplib.Dn}*.

    *@{argument attrs of ldaplib.\_connection.Ldap_Connection.retrieve}*

    :   Abzufragende Textattribute. Per Default werden alle Attribute
        bezogen.

    *@{argument blobs of ldaplib.\_connection.Ldap_Connection.retrieve}*

    :   Abzufragende Binärattribute. Falls alle Attribute bezogen werden
        sollen, müssen die Binärattribute immer explizit spezifiziert
        werden!

    Resultat

    :   Ein Paar der Form (*texts* (Dictionary der Textattribute),
        *blobs* (Dictionary der Binärattribute)), oder *@{identval None}*,
        falls kein Eintrag mit *@{argument base of ldaplib.\_connection.Ldap_Connection.retrieve}*
        existiert.

*@{function Ldap_Connection.change in ldaplib.\_connection}*

:   Ändern der Attribute eines Eintrags.  Attribute können gelöscht
    werden, indem für sie eine leere Liste spezifiziert wird.

    *@{argument base of ldaplib.\_connection.Ldap_Connection.change}*

    :   Basis des zu ändernden Eintrags vom Typ *@{identname Dn in
        ldaplib.Dn}*.

    *@{argument attrs of ldaplib.\_connection.Ldap_Connection.change}*

    :   Zu setzende Textattribute; ein Dictionary von Schlüsseln
        zu Listen von Textwerten.

    *@{argument blobs of ldaplib.\_connection.Ldap_Connection.change}*

    :   Zu setzende Binärattribute. ein Dictionary von Schlüsseln
        zu Listen von Binärwerten.

*@{function Ldap_Connection.remove in ldaplib.\_connection}*

:   Löschen eines Eintrags.

    *@{argument base of ldaplib.\_connection.Ldap_Connection.remove}*

    :   Basis des zu löschenden Eintrags vom Typ *@{identname Dn in
        ldaplib.Dn}*.

*@{function Ldap_Connection.remove_tree in ldaplib.\_connection}*

:   Rekursives, permissives Löschen eines Teilbaums.

    *@{argument base of ldaplib.\_connection.Ldap_Connection.create}*

    :   Basis des zu löschenden Teilbaums vom Typ *@{identname Dn in
        ldaplib.Dn}*.

    Resultat

    :   Liste der DNs der tatsächlich gelöschten Einträge.


## Filter-Anfragen

Filter-Anfragen sind die klassischen LDAP-Anfragen mit einem
Filter-Ausdruck.

*@{function Ldap_Connection.query in ldaplib.\_connection}*

:   Abfrage von LDAP-Datensätzen

    *@{argument base of ldaplib.\_connection.Ldap_Connection.query}*

    :   Basis (Wurzel) der Anfrage vom Typ *@{identname Dn in
        ldaplib.Dn}*.

    *@{argument filter of ldaplib.\_connection.Ldap_Connection.query}*

    :   Filterausdruck vom Typ *@{identname ldaplib.filter}*.

    *@{argument attrs of ldaplib.\_connection.Ldap_Connection.query}*

    :   Gewünschte Textattribute. Per Default werden alle Attribute
        bezogen.

    *@{argument blobs of ldaplib.\_connection.Ldap_Connection.query}*

    :   Gewünschte Binärattribute. Falls alle Attribute bezogen werden
        sollen, müssen die Binärattribute immer explizit spezifiziert
        werden!

    *@{argument timeout of ldaplib.\_connection.Ldap_Connection.query}*

    :   Optionaler anfragespezifischer Timeout.

    Resultat

    :   Ein Iterator über Tripel der Form (*dn* (als Text!), (*texts*
        (Dictionary der Textattribute), *blobs* (Dictionary der
        Binärattribute))). In der Praxis empfiehlt es sich, diesen
        Iterator sogleich in eine explizite Liste zu konvertieren, um
        die notwendige Dauer der LDAP-Verbindung auf ein Minimum zu
        beschränken. Falls auf den *DN*s anschließend explizit gerechnet
        werden muss, empfiehlt sich eine Konvertierung mittels
        *@{function Dn.from_text in ldaplib.Dn}*.

*@{function Ldap_Connection.query_children in ldaplib.\_connection}*

:   Wie *@{identname Ldap_Connection.query in ldaplib.\_connection}*,
    aber das Ergebnis der Anfrage sind die *Kinder* der betreffenden
    LDAP-Objekte, nicht die LDAP-Objekte selbst (korrespondiert zu dem
    primitiven *@{identname ldap.SCOPE_ONELEVEL}*).

*@{function Ldap_Connection.query_tree in ldaplib.\_connection}*

:   Wie *@{identname Ldap_Connection.query in ldaplib.\_connection}*,
    aber das Ergebnis der Anfrage sind die kompletten *Teilbäume* der
    betreffenden LDAP-Objekte (korrespondiert zu dem primitiven
    *@{identname ldap.SCOPE_SUBTREE}*).


## Struktur-Anfragen

Struktur-Anfragen beziehen einen ganzen Teilbaum aus dem LDAP und
präsentieren das Ergebnis als Python-Datenstruktur entsprechend einer
vorgegebenen Struktur.

*@{function Ldap_Connection.query_structure in ldaplib.\_connection}*

:   Abfrage von LDAP-Teilbäumen in strukturierter Form.

    *@{argument base of ldaplib.\_connection.Ldap_Connection.query_structure}*

    :   Basis (Wurzel) des Teilbaums vom Typ *@{identname Dn in
        ldaplib.Dn}*.

    *@{argument struct of ldaplib.\_connection.Ldap_Connection.query_structure}*

    :   Strukturspezifikation vom Typ *@{identname Struct in
        ldaplib.Struct}*. Siehe unten.

    *@{argument timeout of ldaplib.\_connection.Ldap_Connection.query}*

    :   Optionaler anfragespezifischer Timeout.

    Resultat

    :   Eine rekursive Darstellung des LDAP-Baums. Ein Dictionary von
        Schlüsseln zu Werten; ein Schlüssel ist der Name eines
        Attributs, der Wert ist entweder

        * Text (für terminale Textattribute);

        * ein 8-Bit-String (für terminale Binärattribute);

        * ein Dictionary; seine Schlüssel sind Namen von
          Textattributen, seine Werte wiederum Dictionaries, deren
          Schlüssel jeweils den Inhalt des korrespondierenden
          Textattributes darstellen und die zugehörigen Werte die
          korrespondierenden Teilbäume repräsentieren. Jeder Teilbaum
          ist wiederum ein Dictionary, das wiederum rekursiv Schlüssel
          auf Text, 8-Bit-Strings und Teilbäume abbildet.

Die Struktur des Teilbaums wird durch einen Ausdruck vom Typ
*@{identname Struct in ldaplib.Struct}* dargestellt. Dieser ist rekursiv
aufgebaut wie ein LDAP-Baum und spezifiziert, welche Zweige und
Attribute für das Ergebnis betrachtet werden.

*@{function Struct in ldaplib.Struct}*

:   Strukturspezifikation eines LDAP-Teilbaums

    *@{argument object_class of ldaplib.Struct.Struct}*

    :   Optionale Objekt-Klasse. Die Struktur matcht nur auf ein
        Ergebnis, falls der strukturell korrespondierende Knoten die
        spezifizierte Objekt-Klasse hat.

    *@{argument attrs of ldaplib.Struct.Struct}*

    :   Liste von Namen von Textattributen, deren Werte am strukturell
        korrespondierenden Knoten ins Ergebnis übernommen werden.

    *@{argument blobs of ldaplib.Struct.Struct}*

    :   Liste von Namen von Binärattributen, deren Werte am strukturell
        korrespondierenden Knoten ins Ergebnis übernommen werden.

    *@{argument children of ldaplib.Struct.Struct}*

    :   Dictionary von Namen von Textattributen, deren Teilbäume am
        strukturell korrespondierenden Knoten ins Ergebnis übernommen
        werden. Die Werte des Dictionaries geben rekursiv die Struktur
        der Teilbäume als *@{identname Struct in ldaplib.Struct}*s an.


## Vergleichs-Anfragen

Vergleichs-Anfragen berechnen, welche Datensätze und Attribute angepasst
werden müssten, um einen gewünschten Datenbestand herzustellen. Sie
eignen sich dazu, massenhafte Änderungen transparent zu machen.

*@{function Ldap_Connection.diff_tree in ldaplib.\_connection}*

:   Vergleichende Abfrage von LDAP-Teilbäumen.

    *@{argument base of ldaplib.\_connection.Ldap_Connection.diff_tree}*

    :   Basis (Wurzel) des Teilbaums vom Typ *@{identname Dn in
        ldaplib.Dn}*.

    *@{argument blobs of ldaplib.\_connection.Ldap_Connection.diff_tree}*

    :   Folge binärer Attributwerte im Anfragebereich.

    *@{argument wanted of ldaplib.\_connection.Ldap_Connection.diff_tree}*

    :   Gewünschte Datensätze als Folge; jedes Element besteht
        aus einem Tupel der Form *(reldn, (attrs, blobs))*, wobei *reldn*
        der DN relativ zur Wurzel ist (Typ *@{identname Dn in
        ldaplib.Dn}*), *attrs* ein Mapping von Attributname auf eine
        Liste von Werten und *blobs* ein Mapping von Attributname auf
        eine Liste von binären Werten ist.

    Resultat

    :   Ein Tripel *(create, change, remove)* mit Folgen aus
        anzulegenden, zu ändernden und zu löschenden Datensätzen; die
        Reihenfolge der Datensätze ist so, dass sie sequentiell
        abgearbeitet werden können.  Die Elemente in *create* und
        *change* sind Tupel der Form *(dn, (attrs, blobs))*, wobei *dn*
        der DN ist (Typ *@{identname Dn in
        ldaplib.Dn}*), *attrs* ein Mapping von Attributname auf eine
        Liste von Werten und *blobs* ein Mapping von Attributname auf
        eine Liste von binären Werten ist; *remove* besteht einfach aus DNs
        (Typ *@{identname Dn in ldaplib.Dn}*).

*@{function Ldap_Connection.diff_level in ldaplib.\_connection}*

:   Vergleichende Abfrage von LDAP-Teilebenen.

    *@{argument base of ldaplib.\_connection.Ldap_Connection.diff_level}*

    :   Basis (Wurzel) der Teilebene vom Typ *@{identname Dn in
        ldaplib.Dn}*.

    *@{argument filter of ldaplib.\_connection.Ldap_Connection.diff_level}*

    :   Filterausdruck, um Vergleichsmenge einzuschränken.

    *@{argument blobs of ldaplib.\_connection.Ldap_Connection.diff_level}*

    :   Folge binärer Attributwerte im Anfragebereich.

    *@{argument attr_for_dn of ldaplib.\_connection.Ldap_Connection.diff_level}*

    :   Strukturgebendes Attribut für RDNs der Teilebene.

    *@{argument wanted_root of ldaplib.\_connection.Ldap_Connection.diff_level}*

    :   Gewünschte Attribute *(attrs, blobs)* des Elternelements der
        Teilebene.

    *@{argument wanted of ldaplib.\_connection.Ldap_Connection.diff_level}*

    :   Gewünschte Datensätze als Folge; jedes Element besteht
        aus einem Tupel der Form *(name, (attrs, blobs))*, wobei *name*
        der Wert des strukturgebenden Attributs ist, *attrs* ein Mapping
        von Attributname auf eine
        Liste von Werten und *blobs* ein Mapping von Attributname auf
        eine Liste von binären Werten ist.

    Resultat

    :   Ein Tripel *(create, change, remove)* mit Folgen aus
        anzulegenden, zu ändernden und zu löschenden Datensätzen; die
        Reihenfolge der Datensätze ist so, dass sie sequentiell
        abgearbeitet werden können.  Die Elemente in *create* und
        *change* sind Tupel der Form *(dn, (attrs, blobs))*, wobei *dn*
        der DN ist (Typ *@{identname Dn in
        ldaplib.Dn}*), *attrs* ein Mapping von Attributname auf eine
        Liste von Werten und *blobs* ein Mapping von Attributname auf
        eine Liste von binären Werten ist; *remove* besteht einfach aus DNs
        (Typ *@{identname Dn in ldaplib.Dn}*).


## Hilfsmittel zur Datenverarbeitung – *@{identname aux in lhm}*

Die folgenden Funktionen sind hilfreich, wenn Daten aus dem
LDAP verarbeitet oder in eine Form gebracht werden sollen, in der sie
in Textdateien abgelegt werden können:

*@{function some_elem in ldaplib.aux}*

:   Element aus einer Liste auswählen; bei leeren Listen wird
    ein Default zurückgegeben.

*@{function the_value in ldaplib.aux}*

:   Eindeutiges Element anhand seines Schlüssel aus einem Mapping
    nach Listen auswählen.  Mehrdeutige Elemente liefern Fehler;
    ebenso nicht existierende Elemente, sofern kein ausdrücklicher
    Default angegeben ist.

*@{function boolify in ldaplib.aux}*

:   Booleschen Ausdruck in eine kanonische Textdarstellung überführen.

*@{function partial_dict in ldaplib.aux}*

:   Fast wie *@{identname dict}*; Schlüssel, die auf *@{identval None}* abgebildet
    werden, erscheinen nicht im Ergebnis.


# Intermezzo - Config-Spaces in *@{identname Config_Space in lhm.Config_Space}*

Im Kontext der LHM kommt es häufiger vor, dass Konfigurations-Daten (z.
B. aus dem LDAP) lokal im Dateisystem in einem bestimmten Format
abgelegt werden, um ein Tool (z. B. FAI) zu konfigurieren.  Dieses
Konzept wird traditionell als Config-Space bezeichnet.  Die Klasse
*@{identname Config_Space in lhm.Config_Space}* in Modul *@{identname
lhm.Config_Space}* bietet die Möglichkeit, strukturiert solche
Config-Spaces zusammenzustellen und im Dateisystem abzulegen.  Damit
kann z. B. der Config-Space für FAI aus den FAI-Klassen
erstellt werden.

Ein Config-Space ist ein Mapping von (relativen) Dateinamen auf
Inhalte (Text oder 8-Bit-String).  Ein Dateiname wird dabei als
die Liste seiner Pfadkomponenten identifiziert.


*@{function Config_Space in lhm.Config_Space}*

:   Leerer Config-Space.

*@{function Config_Space.augment_text in lhm.Config_Space}*

:   Config-Space angereichert mit Zeilen einer Textdatei.

    *@{argument loc of lhm.Config_Space.Config_Space.augment_text}*

    :   Pfadkomponenten des relativen Dateinamens.

    *@{argument lines of lhm.Config_Space.Config_Space.augment_text}*

    :   Hinzuzufügende Textzeilen.  Fehler, falls an der spezifizierten
        Position bereits eine Binärdatei liegt.

*@{function Config_Space.augment_nonempty_text in lhm.Config_Space}*

:   Config-Space angereichert mit Zeilen einer Textdatei. Verhält sich
    wie @{function Config_Space.augment_text in lhm.Config_Space}*,
    außer wenn *@{argument lines of lhm.Config_Space.Config_Space.augment_text}*
    leer ist: dann wird der Config-Space unverändert zurückgeliefert.

*@{function Config_Space.place_blob in lhm.Config_Space}*

:   Config-Space angereichert mit Binärdatei.

    *@{argument loc of lhm.Config_Space.Config_Space.place_blob}*

    :   Pfadkomponenten des relativen Dateinamens.

    *@{argument blob of lhm.Config_Space.Config_Space.place_blob}*

    :   Zu vermerkender Inhalt.  Fehler, falls an der spezifizierten
        Position bereits eine Binär- oder Textdatei liegt.

    *@{argument executable of lhm.Config_Space.Config_Space.place_blob}*

    :   Hinweis, dass die Binärdatei ausführbar sein soll.

*@{function Config_Space.merge in lhm.Config_Space}*

:   Config-Space vereinigt mit weiterem Config-Space.

    *@{argument other of lhm.Config_Space.Config_Space.merge}*

    :   Weiterer Config-Space; sinngemäß wird dessen Inhaltshistorie
        iterativ auf das implizite Argument ergänzt; bei Konflikten
        setzt sich der Inhalt aus *@{argument other of lhm.Config_Space.Config_Space.merge}*
        durch.

*@{function Config_Space.serialize in lhm.Config_Space}*

:   Iterator über den gesamten Inhalt des Config-Spaces;  jede Iteration
    liefert ein Paar aus Dateiname und (binär kodiertem) Inhalt.

*@{function Config_Space.dump in lhm.Config_Space}*

:   Ausgabe des gesamten Inhalts des Config-Spaces ins Dateisystem.
    Für ausführbare Dateien werden die betreffenden Berechtigungen
    gesetzt.

    *@{argument base of lhm.Config_Space.Config_Space.dump}*

    :   Wurzelverzeichnis.  Dies darf nicht bereits existieren.


# Fachliche Abstraktion

Das Modul *@{identname lhm}* ist eine fachliche Schnittstelle zu den
grundlegenden Daten, die im LDAP der LHM hinterlegt sind: Benutzerdaten,
Objektdaten, und Releasedaten. An vielen Stellen beschränkt sich dies
bislang auf grundlegendes, kann aber in Zukunft ausgebaut werden.


## Administrative Bereiche – *@{identname lhm.unit}* und *@{identname lhm.organization}*

Die LHM ist in unterschiedliche administrative Bereiche gegliedert, und
dies spiegelt sich im LDAP wieder.

Die einem administrativen Bereich zugehörigen Identifikations- und
Metadaten sind in einem eigenen Typ *@{identname Unit in lhm.unit}*
repräsentiert. Dieser ist *opaque*, d. h. Anwendungen sollten Werte
dieses Typs nur durchreichen, aber nicht selbst verändernd eingreifen,
um robust gegen künftige Änderungen zu sein.

Jeder Wert vom Typ *@{identname Unit in lhm.unit}* enthält einen
Teilwert vom Typ *@{identname Organization in lhm.organization}*, der
die Gesamtorganisation repräsentiert, z. B. eine universelle Basis aller
administrativen Bereiche und Spezifika des LDAP-Schemas.

Das lokale System kann mittels des Aufrufs *@{executable
sbin/create-unit-config}* auf einen bestimmten administrativen Bereich
konfiguriert werden.

*@{function lhm.unit.from_system}*

:   Liefert einen Wert vom Typ *@{identname Unit in lhm.unit}*, der den
    administrativen Bereich beschreibt, innerhalb dessen sich das laufende
    System befindet.

*@{function lhm.unit.filter}*

:   Liefert zu einem gegebenen administrativen Bereich einen
    LDAP-Filter.

*@{function lhm.unit.pick}*

:   Liefert für eine Organisationskonfigurationdatei und den Kurzbezeichner
    eines administrativen Bereiches eine passende LDAP-Konfiguration
    vom Typ *@{identname Config in ldaplib}* und einen administrativen
    Bereich vom Typ *@{identname Unit in lhm.unit}*; falls ein Argument
    *@{identval None}* ist, wird der entsprechende Default vom System
    verwendet.

*@{function lhm.unit.obtain_finder}*

:   Liefert eine Funktion, die einen DN auf den zugehörigen administrativen Bereich
    abbildet, falls vorhanden. Dies ist eine höherstufige Konstruktion,
    da für diese Abbildung eine Suchdatenstruktur initial aufgebaut
    werden muss. Trotz Suchdatenstruktur ist die Implementierung nicht
    auf Effizienz optimiert.

*@{function lhm.unit.obtain_marker}*

:   Liefert eine Funktion ähnlich *@{identname dict}*, die einen
    LDAP-Datensatz mit den notwendigen Attributen für ein Tagging
    auf einen bestimmten administrativen Bereich ergänzt.


## Exkurs: LDAP-Struktur eines administrativen Bereichs

Diese Beschreibung gibt einen groben Überblick zum Zeitpunkt
der Niederschrift (Dez. 2017).  Sie ist dabei weder vollständig noch
kann garantiert werden, dass sie in Zukunft Gültigkeit behalten wird.
Sie kann allerdings die Orientierung zum Einstieg vereinfachen.

Unterhalb der Wurzel eines administrativen Bereiches gibt es
verschiedene Pockets (»Datentaschen«), die durch *ou*s repräsentiert
sind.  Je nach Typ (*objectClass*) werden Objekte durch GOsa² in
unterschiedlichen Pockets abgelegt.

Die meisten lesenden Abfragen stützen sich per Filter auf *objectClass*
ab und ignorieren die Pockets.  Dennoch ist es nicht spezifiziert, was
passiert, wenn die Sortierung in Pockets verletzt wird: unsere Software
als ganzes geht davon aus, dass sie konsequent durchgehalten wird.

Die konkrete Unterteilung der Pockets und ihre *ou*s sind historisch
gewachsen und nicht immer sinnfällig.  Die konkreten *ou*s sind
als Attribute in *@{identname Organization in lhm.organization}*
repräsentiert.

Bei Releasedaten liegen in den Pockets nicht nur eine Menge von Objekten,
sondern sogenannte Release-Teilbäume, die den Namensraum aller Releases
als *ou* nachbilden (z. B. *ou=6.0.0,ou=walhalla*).

Die folgende Tabelle gibt eine Übersicht:

Attribut in *@{identname Organization in lhm.organization}*        Pocket                                 Verwendung                   charakteristische Typen
-----------------------------------------------------------------  -------------------------------------  ---------------------------  ------------------------------------------------------------------------
*@{identname user_infix in lhm.organization.Organization}*         *ou=users*                             Benutzer                     *posixAccount*
*@{identname group_infix in lhm.organization.Organization}*        *ou=groups,ou=limux*                   Posix-Gruppen                *posixGroup* und *gosaApplicationGroup*, letztere mit Release-Teilbäumen
*@{identname group_infix in lhm.organization.Organization}*        *ou=groups,ou=limux*                   Objektgruppen                *gosaGroupOfNames*
*@{identname server_infix in lhm.organization.Organization}*       *ou=servers,ou=systems,ou=limux*       Server                       *goServer*
*@{identname workstation_infix in lhm.organization.Organization}*  *ou=workstations,ou=systems,ou=limux*  Clients                      *gotoWorkstation*
*@{identname printer_infix in lhm.organization.Organization}*      *ou=printers,ou=systems,ou=limux*      Drucker                      *gotoPrinter*
*@{identname device_infix in lhm.organization.Organization}*       *ou=devices,ou=limux*                  USB-Geräte                   *gotoDevice*
*@{identname incoming_infix in lhm.organization.Organization}*     *ou=incoming,ou=limux*                 frisch registrierte Clients
*@{identname terminal_infix in lhm.organization.Organization}*     *ou=terminals,ou=systems,ou=limux*     (schon lange ohne Funktion)
*@{identname fai_infix in lhm.organization.Organization}*          *ou=fai,ou=configs,ou=systems*         FAI-Klassen                  Release-Teilbäume
*@{identname app_infix in lhm.organization.Organization}*          *ou=apps*                              GOsa²-Anwendungen            *gosaApplication* und Release-Teilbäume
*@{identname acl_infix in lhm.organization.Organization}*          *ou=aclroles,ou=limux*                 Rechte in GOsa²              *gosaRole*
*@{identname gosa_infix in lhm.organization.Organization}*         *ou=gosa,ou=configs,ou=systems*        Sitzungen in GOsa²            *gosaLockEntry*


## Benutzer – *@{identname lhm.user}*

*@{function lhm.user.get}*

:   Liefert für einen administrativen Bereich die Daten eines Benutzer.
    Gruppenzugehörigkeiten werden automatisch
    aufgelöst, Benutzerattribute haben Vorrang vor Gruppenattributen.
    Binärattribute werden über ein eigenes Argument identifiziert und in
    einem eigenen Datensatz zurückgeliefert.

*@{function lhm.user.get_group_members}*

:   Liefert für einen administrativen Bereich die Benutzer einer
    Gruppe.  Ein einschränkender Filter kann mit angegeben werden.

*@{function lhm.user.get_groups_of}*

:   Liefert für einen administrativen Bereich die Gruppen eines
    Benutzer.  Ein einschränkender Filter kann mit angegeben werden.


## Systeme – *@{identname lhm.system}*

*@{function lhm.system.get_by_hostname}*

:   Liefert für einen administrativen Bereich die Daten eines Systems
    (Rechner).  Das System wird per Hostname im LDAP (!) identifiziert.
    Objektgruppenzugehörigkeiten werden automatisch aufgelöst,
    Objektattribute haben Vorrang vor Gruppenattributen. Binärattribute
    werden über ein eigenes Argument identifiziert und in einem eigenen
    Datensatz zurückgeliefert.

*@{function lhm.system.get_by_sid_mac}*

:   Liefert für einen administrativen Bereich die Daten eines Systems
    (Rechner).  Das System wird per SID (MAC im LDAP) identifiziert.
    Objektgruppenzugehörigkeiten werden automatisch aufgelöst,
    Objektattribute haben Vorrang vor Gruppenattributen. Binärattribute
    werden über ein eigenes Argument identifiziert und in einem eigenen
    Datensatz zurückgeliefert.

*@{function lhm.system.split_host_name}*

:   Liefert für einen qualifizierten Rechnernamen
    das Paar aus Basisname und Domain, für einen unqualifizierten
    Rechnername das Paar aus Basisname und *@{identval None}*;
    traditionell kommt beides im LDAP der LHM vor.


## LiMux-Releases – *@{identname lhm.release}*

Das Modul *@{identname lhm.release}* beschreibt die Kernidentität eines
Releases: seinen Namen.  Releasename können Trenner »/« enthalten, die
den Namen in Komponenten untergliedern. Sind die Komponenten eines
Releases A/…/B ein Präfix der Komponenten eines Release A/…/B/…/C, stammt
Release A/…/B/…/C von Release A/…/B ab.

*@{function lhm.release.split}*

:   Zerlegt einen Releasenamen in seine Komponenten.

*@{function lhm.release.join}*

:   Erstellt einen Releasenamen aus seinen Komponenten.

*@{function lhm.release.attach}*

:   Ergänzt einen DN mit releasetypischen OUs.

*@{function lhm.release.filename_of}*

:   Liefert zu einem Releasenamen einen eindeutigen Identifikator, der
    als Dateiname verwendet werden kann.

*@{function lhm.release.of_filename}*

:   Umkehrfunktion zu *@{function lhm.release.filename_of}*.
