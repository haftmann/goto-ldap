# Copyright (c) 2008 - 2009 Landeshauptstadt München
#
# Author: Jan-Marek Glogowski
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package GOsa::Common;

require 5.6.0;
use strict;
use warnings;

use Net::LDAP;
use Net::LDAP::Constant qw(LDAP_SUCCESS LDAP_NO_SUCH_OBJECT LDAP_REFERRAL);
use File::Basename;
use POSIX;
use Cwd qw(abs_path);
use URI;

BEGIN
{
  use Exporter ();
  our $VERSION = '2009-02-25_01';
  our @ISA = qw(Exporter);

  our %EXPORT_TAGS = (
    'ldap' => [qw(
      &gosa_ldap_parse_config
      &gosa_ldap_parse_config_ex
      &gosa_ldap_rsearch
      &gosa_ldap_split_dn
      &gosa_ldap_init
    )],
    'misc' => []
  );

  Exporter::export_ok_tags(keys %EXPORT_TAGS);
}

sub gosa_ldap_parse_config_ex
{
  my %result = ();

  my $ldap_info = '/etc/ldap/ldap-shell.conf';

  if (!open( LDAPINFO, '<', ${ldap_info} ))
  {
     warn "Couldn't open ldap info ($ldap_info): $!\n";
     return undef;
  }
  while(<LDAPINFO>) {
    if (/^([A-Z0-9a-z_]+)=(["']?)(.*)\2\s*(?:#.*)?$/) {
      if ($1 eq "LDAP_URIS") {
        my @uris = split(/ /, $3);
        $result{$1} = \@uris;
      }
      else {
        $result{$1} = $3;
      }
    }
  }
  close(LDAPINFO);
  if (not exists($result{"LDAP_URIS"}))
  {
    warn "LDAP_URIS missing in file $ldap_info\n";
  }
  return \%result;
}

#------------------------------------------------------------------------------

sub gosa_ldap_parse_config
{
  my ($ldap_config) = @_;

  if (! defined $ldap_config) {
    $ldap_config = '/etc/ldap/ldap.conf';
  }

  return unless -r $ldap_config;

  # Read LDAP file if it's < 100kB
  return if( (-s "${ldap_config}" > 100 * 1024) 
          || (! open( LDAPCONF, '<', ${ldap_config} )) );

  my @content = <LDAPCONF>;
  close( LDAPCONF );

  my( $ldap_base, @ldap_uris, $ldap_bind_dn );

  # Parse LDAP config
  foreach my $line (@content) {
    $line =~ /^\s*(#|$)/ && next;
    chomp($line);

    if ($line =~ /^BASE\s+(.*)$/i) {
      $ldap_base= $1;
    }
    elsif ($line =~ m#^URI\s+(.*)\s*$#i ) {
      my (@ldap_servers) = split( ' ', $1 );
      foreach my $server (@ldap_servers) {
        push( @ldap_uris, $1 )
          if( $server =~ m#^(ldaps?://([^/:\s]+)(:([0-9]+))?)/?$#i );
      }
    }
  }

  return ($ldap_base, \@ldap_uris, undef, $ldap_config);
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#
# Common LDAP initialization routine
#
# $ldap_conf      = LDAP config file - may be undef
# $prompt_dn      = Prompt user for bind dn if true
# $bind_dn        = Use DN to bind to LDAP server
# $prompt_pwd     = Prompt user for bind password if true
# $bind_pwd       = Use password to bind to LDAP server
# $obfuscate_pwd  = Show stars instead omiting echo
#
# Returns a hash of results
#  'BASE'    => LDAP search base from config
#  'URIS'    => LDAP server URIs from config
#  'HANDLE'  => Net::LDAP handle
#  'BINDDN'  => Bind DN from config or prompt
#  'BINDPWD' => Bind password from prompt
#  'CFGFILE' => Config file used
#
# These values are just filled, if they weren't provided,
# i.e. 
#
sub gosa_ldap_init {
  my( $ldap_conf, $prompt_dn, $bind_dn, 
      $prompt_pwd, $bind_pwd, $obfuscate_pwd , $bindtimeout ) = @_;
  my %results;
  $bindtimeout = 60 unless(defined($bindtimeout));
  $bindtimeout = 2 if ($bindtimeout < 2);
  # Parse ldap config
  my ($base,$ldapuris,$binddn,$file) = gosa_ldap_parse_config( $ldap_conf );
  return( sprintf( "LDAP config parsing error (%i)", __LINE__ ) )
    if( ! defined $base );

  $binddn = $bind_dn if( defined $bind_dn );
  %results = ( 'BASE' => $base, 'URIS' => $ldapuris, 'BINDDN' => $binddn, 'timeout' => $bindtimeout );
  $results{ 'CFGFILE' } = $file if( $file ne $ldap_conf );

  return( "Couldn't find LDAP base in config!" ) if( ! defined $base );
  return( "Couldn't find LDAP URI in config!" ) if( ! defined $ldapuris );

  # Create handle
  my $ldap = Net::LDAP->new( $ldapuris ) ||
    return( sprintf( "LDAP 'new' error: %s (%i)", $@, __LINE__ ) );
  $results{ 'HANDLE' } = $ldap;

  # Prompt for DN
  if( (! defined $bind_dn) && (defined $prompt_dn && $prompt_dn) )
  {
    $| = 1;
    print( 'Bind DN: ' );
    $| = 0;
    $bind_dn = <STDIN>;
    $results{ 'BINDDN' } = $bind_dn;
  }

  my $mesg;
  if( defined $bind_dn ) {
    if( defined $bind_pwd ) {
      $mesg = $ldap->bind( $binddn, password => $bind_pwd );
    }
    elsif( defined $prompt_pwd ) {
      # Prompt for password

      $| = 1;
      print( 'Password: ' );
      $| = 0;
      $bind_pwd = '';

      # Disable terminal echo
      system "stty -echo -icanon";

      my $inchr;
      while (sysread STDIN, $inchr, 1) {
        if (ord($inchr) < 32) { last; }
        $bind_pwd .= $inchr;
        syswrite( STDOUT, "*", 1 ) # print asterisk instead
          if( defined $obfuscate_pwd && $obfuscate_pwd );
      }
      system "stty echo icanon";

      $results{ 'BINDPWD' } = $bind_pwd;

      $mesg = $ldap->bind( $binddn, password => $bind_pwd );
    }
    else { $mesg = $ldap->bind( $binddn ); }
  }
  else { $mesg = $ldap->bind(); } # Anonymous bind

  return( "LDAP bind error: " . $mesg->error . ' (' . $mesg->code . ")\n" )
    if( LDAP_SUCCESS != $mesg->code );

  return \%results;
}


#
# Split the dn (works with escaped commas)
#
# $dn = The DN to split
#
# Return an array of RDNs
#
sub gosa_ldap_split_dn {
  my ($dn) = @_;

  # Split at comma
  my @comma_rdns = split( ',', $dn );
  my @result_rdns = ();
  my $line = '';

  foreach my $rdn (@comma_rdns) {
    # Append rdn to line
    if( '' eq $line ) { $line = $rdn; }
    else { $line .= ',' . $rdn; }

    # Count the backslashes at the end. If we have even length
    # of $bs add to result array and set empty line
    my($bs) = $rdn =~ m/([\\]+)$/;
    $bs = "" if( ! defined $bs );
    if( 0 == (length($bs) % 2) ) {
      push( @result_rdns, $line );
      $line = "";
    }
  }

  return( @result_rdns );
}

#
# Common checks for forward and reverse searches
#
sub gosa_ldap_search_checks {
  my( $base, $sbase ) = (@_)[1,2];

  if( scalar @_ < 3 ) {
    warn( "gosa_ldap_search needs at least 3 parameters" );
    return;
  };

  if( defined $sbase && (length($sbase) > 0) ) {
    # Check, if $sbase is a base of $base
    if( $sbase ne substr($base,-1 * length($sbase)) ) {
      warn( "gosa_ldap_search: (1) '$sbase' isn't the base of '$base'" );
      return;
    }

    $base = substr( $base, 0, length( $base ) - length( $sbase ) );

    # Check, if $base ends with ',' after $sbase strip
    if( ',' ne substr( $base, -1 ) ) {
      warn( "gosa_ldap_search: (2) '$sbase' isn't the base of '$base'" );
      return;
    }
    $base  = substr( $base, 0, length($base) - 1 );
    $sbase = ',' . $sbase;
  }
  else { $sbase = ''; }

  return( $base, $sbase );
}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#
# $ldap    = Net::LDAP handle
# $base    = Search base ( i.e.: ou=test,ou=me,ou=very,ou=well )
# $sbase   = Stop base ( i.e.: ou=very,ou=well )
# $filter  = LDAP filter
# $scope   = LDAP scope
# $subbase = On every $base look into $subbase,$base ( i.e.: ou=do )
# $attrs   = Result attributes
#
# Example searches in:
#   ou=do,ou=test,ou=me,ou=very,ou=well
#   ou=do,ou=me,ou=very,ou=well
#   ou=do,ou=very,ou=well
#
# Returns (Net::LDAP::Search, $search_base) on LDAP failure
# Returns (Net::LDAP::Search, $search_base) on success
# Returns undef on non-LDAP failures
#
sub gosa_ldap_rsearch {
  use Switch;

  my ($ldap,$base,$sbase,$filter,$scope,$subbase,$attrs) = @_;

  ( $base, $sbase ) = gosa_ldap_search_checks( @_ );
  return if( ! defined $base );

  my (@rdns,$search_base,$mesg);

  @rdns = gosa_ldap_split_dn( $base );
  return if( 0 == scalar @rdns );

  while( 1 ) {

    # Walk the DN tree
    switch( scalar @rdns ) {
    case 0 {
      # We also want to search the stop base, if it was defined
      return if( ! defined $sbase );
      if( length( $sbase ) > 0 )
        { $search_base = substr( $sbase, 1 ); }
      else { $search_base = ''; }
      undef( $sbase );
      }
    else {
      $search_base = join( ',', @rdns ); 
      shift(@rdns);
      $search_base .= $sbase;
      }
    }

    # Initialize hash with filter
    my %opts = ( 'filter' => $filter );

    # Set searchbase
    if( defined $subbase && $subbase ) 
      { $opts{ 'base' } = "${subbase},${search_base}" }
    else { $opts{ 'base' } = "${search_base}" }

    # Set scope
    $opts{'scope'} = $scope if (defined $scope && $scope);
    $opts{'attrs'} = $attrs if (defined $attrs);

    # LDAP search

    # The referral chasing is much simpler then the OpenLDAP one.
    # It's just single level support, therefore it can't really
    # chase a trail of referrals, but will check a list of them.

    my @referrals;
    my $chase_referrals = 0;
RETRY_SEARCH:
    $mesg = $ldap->search( %opts );

    if( LDAP_REFERRAL == $mesg->code ) { # Follow the referral
      if( ! $chase_referrals ) {
        my @result_referrals = $mesg->referrals();
        foreach my $referral (@result_referrals) {
          my $uri = new URI( $referral );
          next if( $uri->dn ne $opts{ 'base' } ); # But just if we have the same base
          push( @referrals, $uri );
        }
        $chase_referrals = 1;
      }

NEXT_REFERRAL:
      next if( ! scalar @referrals );
      my $uri = new URI( $referrals[ 0 ] );
      $ldap = new Net::LDAP( $uri->host );
      @referrals = splice( @referrals, 1 );
      goto NEXT_REFERRAL if( ! defined $ldap );
      $mesg = $ldap->bind();
      goto NEXT_REFERRAL if( LDAP_SUCCESS != $mesg->code );
      goto RETRY_SEARCH;
    }
    if( LDAP_NO_SUCH_OBJECT == $mesg->code ) { # Ignore missing objects (32)
      goto NEXT_REFERRAL if( scalar @referrals );
      next;
    }

    return $mesg if( $mesg->code ); # Return undef on other failures

    last if( $mesg->count() > 0 );
  }

  return( $mesg, ${search_base} );
}


END {}

1;

__END__

# vim:ts=2:sw=2:expandtab:shiftwidth=2:syntax:paste
