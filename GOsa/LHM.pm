# Copyright (c) 2008 - 2009 Landeshauptstadt München
#
# Author: Jan-Marek Glogowski
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package GOsa::LHM;

use strict;
use warnings;

use Net::LDAP;
use Switch;
use GOsa::Common qw(:ldap);

BEGIN
{
  use Exporter ();
  use vars qw(%EXPORT_TAGS @ISA $VERSION);
  $VERSION = '20070329';
  @ISA = qw(Exporter);

  %EXPORT_TAGS = (
    'gosa' => [qw(
      &lhm_gosa_get_adm_units
      &lhm_gosa_get_adm_unit_by_shortname
    )],
    'parse_organization' => [qw(
      &parse_organization
    )]
  );

  Exporter::export_ok_tags(keys %EXPORT_TAGS);
}


my $lhm_conf_default = '/etc/goto-ldap/lhm.conf';
my $unit_root_filter = '(&(objectClass=lhmOrganizationalUnit)(objectClass=gosaAdministrativeUnit))';

sub parse_organization {
  my $lhm_conf = @_ == 0 ? $lhm_conf_default : $_[0];
  open(LDAPCONF, '<', $lhm_conf) or die "Could not open $lhm_conf";
  my @content = <LDAPCONF>;
  close(LDAPCONF);
  my %result = ();
  foreach my $line (@content) {
    $line =~ /^\s*(#|$)/ && next;
    chomp($line);
    if ($line =~ /^\s*(\S+)\s+(.*)$/) {
      my ($key, $value) = ($1, $2);
      $result{lc($key)} = $value;
    }
  }
  return \%result;
}

# Put correct reference in all hashes
sub augment_unit_data {
  my ($org, $entry, $fai, $shortnames, $dns, $ous) = @_;
    # $shortnames, $dns, $ous result by reference

  my $dn = $entry->dn();
  my $ou = $entry->get_value('ou');
  my $description = $entry->get_value('description');

  my $tag = $entry->get_value('gosaUnitTag');

  my %result = (
    'dn' => $dn,
    'tag' => $tag,
    'fai' => $fai,
    'lhm' => $ou,
    'description' => $description
  );

  $shortnames->{$ou} = \%result;
  $dns->{$dn} = \%result;
  $ous->{$ou} = \%result;
}


sub lhm_gosa_get_adm_units {

  my ($org, $ldap) = @_ == 1 ? (parse_organization(), $_[0]) : @_;
  my $lhm_base = $org->{'base'};
  my $incoming_infix = $org->{'incoming_infix'};

  my $mesg = $ldap->search(
    base => $lhm_base,
    filter => $unit_root_filter,
    scope => 'sub',
    attrs => ['dn', 'ou', 'gosaUnitTag', 'description']
  );
  return( "LDAP search error: " . $mesg->error . ' (' . $mesg->code . ")\n" )
    if( 0 != $mesg->code );

  my (%shortnames, %dns, %ous);
  foreach my $entry ($mesg->entries()) {
    augment_unit_data($org, $entry, undef, \%shortnames, \%dns, \%ous);
  }

  # We can't put it all into one LDAP query;
  # hence an overapproximation first…
  my @incoming_fragments = split(',', $incoming_infix);
  $mesg = $ldap->search(
    base => $lhm_base,
    filter => "(&(objectClass=organizationalUnit)($incoming_fragments[0]))",
    scope => 'sub',
    attrs => ['dn']
 );
  return( "LDAP search error: " . $mesg->error . ' (' . $mesg->code . ")\n" )
    if( 0 != $mesg->code );

  # and then a more selective filtering
  foreach my $entry ($mesg->entries()) {
    next unless ($entry->dn() =~ /^\Q$incoming_infix\E,/);
    my $unitdn = substr($entry->dn(), length($incoming_infix . ','));

    if (exists $dns{$unitdn}) {
      $dns{$unitdn}->{'fai'} = $unitdn;
      next;
    }

    my $info = $ldap->search(
      base => $unitdn,
      filter => $unit_root_filter,
      scope => 'base',
      attrs => ['dn', 'ou', 'gosaUnitTag', 'description']);
    return( "LDAP search error: " . $mesg->error . ' (' . $mesg->code . ")\n" )
      if( 0 != $mesg->code );
    next if( 1 != $info->count() );

    my $entry = ($info->entries())[0];
    augment_unit_data($org, $entry, $entry->dn(), \%shortnames, \%dns, \%ous);
  }

  # Fill all FAI references
  foreach my $dn (keys %dns) {
    if( ! defined $dns{ $dn }->{ 'fai' } ) {
      my $longest = '';
      foreach my $faidn (keys %dns) {
        if( defined $dns{ $faidn }->{ 'fai' }
          && (${dn} =~ /$dns{ $faidn }->{ 'fai' }$/)
          && (length($dns{ $faidn }->{ 'fai' }) > length($longest) ))
        {
          $longest = $dns{ $faidn }->{ 'fai' };
        }
      }
      $dns{ $dn }->{ 'fai' } = $longest;
    }
  }

  return( \%shortnames, \%dns, \%ous );
}


sub lhm_gosa_get_adm_unit_by_shortname {

  my ($org, $ldap, $shortname) = @_ == 2 ? (parse_organization(), $_[0], $_[1]) : @_;
  my $lhm_base = $org->{'base'};
  my $incoming_infix = $org->{'incoming_infix'};

  my @candidates = ();
  my $mesg = $ldap->search(
    base => "ou=$shortname,$lhm_base",
    filter => $unit_root_filter,
    scope => 'base',
    attrs => ['dn', 'ou', 'gosaUnitTag', 'description']
  );
  return ("LDAP search error: " . $mesg->error . ' (' . $mesg->code . ")\n" )
    if ($mesg->code != 0 && $mesg->code != 32);
  push(@candidates, ($mesg->entries()));

  switch (@candidates) {
    case 0 { return( "No department with shortname '$shortname' found" ); }
    case 1 { ; }
    else {
      return( "Multiple department with shortname '$shortname' found ("
        . @candidates . ")\n" );
    }
  }

  my $entry = $candidates[0];
  my ($result, $incoming_base) = gosa_ldap_rsearch($ldap, $entry->dn(),
    $lhm_base, '(objectClass=organizationalUnit)', 'base', $incoming_infix);
  my (%shortnames, %dns, %ous);
  augment_unit_data($org, $entry, $incoming_base, \%shortnames, \%dns, \%ous);

  return (\%shortnames, \%dns, \%ous);
}


END {}

1;

__END__

# vim:ts=2:sw=2:expandtab:shiftwidth=2:syntax:paste
